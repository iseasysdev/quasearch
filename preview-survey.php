<?php include "header.php";?>
<?php include "nav.php";?>
<div class="container">
    <div class="row">
        <!-- start: Content -->
        <!-- start: Content -->
        <div id="content" class="col-sm-12 padding0">
            <div class="form-member">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><span class="glyphicons group"><i></i></span>Survey</h3>
                            </div>
                            <div class="panel-body">
                                <h2>Survey Name</h2>
                                <form action="/" method="post">
                                    <h3><small>Short Description of the survey here</small></h3>
                                    <div class="well">
                                        <div class="row">
                                            <div class="form-group">
                                                <label>
                                                    Family Name
                                                </label>
                                                <input type="text" class="col-lg-12 form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    First Name
                                                </label>
                                                <input type="text" class="col-lg-12 form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    Middle Name
                                                </label>
                                                <input type="text" class="col-lg-12 form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>
                                                    Suffix
                                                </label>
                                                <input type="text" class="col-lg-12 form-control">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<?php include "footer.php";?>