<?php
App::uses('ReservationsController', 'Controller');

/**
 * ReservationsController Test Case
 *
 */
class ReservationsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.reservation',
		'app.member',
		'app.group',
		'app.user',
		'app.team',
		'app.project',
		'app.login_log',
		'app.developer',
		'app.incentive',
		'app.sale'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

}
