<?php
App::uses('OptionItem', 'Model');

/**
 * OptionItem Test Case
 *
 */
class OptionItemTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.option_item',
		'app.option'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->OptionItem = ClassRegistry::init('OptionItem');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->OptionItem);

		parent::tearDown();
	}

}
