<?php
App::uses('Developer', 'Model');

/**
 * Developer Test Case
 *
 */
class DeveloperTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.developer',
		'app.reservation',
		'app.member',
		'app.group',
		'app.user',
		'app.team',
		'app.project',
		'app.login_log',
		'app.incentive',
		'app.sale'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Developer = ClassRegistry::init('Developer');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Developer);

		parent::tearDown();
	}

}
