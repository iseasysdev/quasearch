<?php
App::uses('UsersController', 'Controller');
/**
 * Dashboard Controller
 *
 * @property Member $Member
 * @property PaginatorComponent $Paginator
 */
class DashboardController extends UsersController {
	public $uses = null;

	public function index(){}
}
