<?php
App::uses('AppController', 'Controller');
/**
 * Surveys Controller
 *
 * @property Survey $Survey
 * @property PaginatorComponent $Paginator
 */
class SurveysController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

	function beforeFilter() {
		parent::beforeFilter();		
		
	} 

	public function perform($id = null) {
		

		if (!$this->Survey->exists($id)) {
			throw new NotFoundException(__('Invalid survey'));
		}

		if ($this->request->is('post')) {
			$this->loadModel('Submission');
			$this->loadModel('QuestionAnswer');
			$this->loadModel('QuestionItemAnswer');

			$submission['survey_id'] = 1;//$this->request->data['survey_id']l
			$submission['user_id'] = $this->Auth->user('id');//$this->request->data['survey_id']l
			$submission['raw_data'] = json_encode($this->request->data);
			$submission['latitude'] = $this->request->data['latitude'];
			$submission['longitude'] = $this->request->data['longitude'];
			$submission['map_api_url'] = $this->request->data['map_api_url'];
			$submission['status'] = 'active';
			$this->Submission->create();
			if($this->Submission->save($submission)){
				/*
				$question_answers = array();
				foreach ($this->request->data['answer'] as $question_id => $answer) {
					$question_answer['submission_id'] = $this->Submission->id;
					if(! is_array($answer)){
						$question_answer['question_id'] = $question_id;
						$question_answer['value'] = $answer;
						$this->QuestionAnswer->create();
						$this->QuestionAnswer->save($question_answer);
					}else{
						//Multiple Items - Question
						//$question_item_answer['question_id'] = $question_id;
						$question_item_answer['submission_id'] = $this->Submission->id;
						foreach ($answer as $question_item_id => $question_item_data) {
							$question_item_answer['question_item_id'] = $question_item_id;
							foreach ($question_item_data as $option_id => $answer) {
								
								if($answer=='Yes'){
									$option_item = ClassRegistry::init('OptionItem')->read(null,$option_id);
									$question_item_answer['option_id'] = $option_id;
									$question_item_answer['value'] = $option_item['OptionItem']['value'];
									$this->QuestionItemAnswer->create();
									$this->QuestionItemAnswer->save($question_item_answer);
								}
							}
						}

					}
					
				}*/			

				$this->Session->setFlash(__('The survey has been saved.'));
				return $this->redirect(array('action' => 'index'));

			}else{
				pr($this->Submission->validationErrors);
			}
			
			exit();
		}

		$this->Survey->contain(array('SurveyForm'=>array('order'=>array('SurveyForm.sequence_no'=>'ASC'))));
		$this->Survey->contain(array('SurveyForm'=>array('conditions'=>array('SurveyForm.parent_id <'=>1),'order'=>array('SurveyForm.sequence_no'=>'ASC'),'ChildSurveyForm')));
		$survey = $this->Survey->read(null,$id);		
		$this->set(compact('survey'));
		$steps = array();
		foreach ($survey['SurveyForm'] as $key => $form) {
			$forms[$key] = $this->Survey->getForm($form['id']);
			if(count($form['ChildSurveyForm']) > 0){
				foreach ($form['ChildSurveyForm'] as $subform) {
					$forms[$key]['forms'][] = $this->Survey->getForm($subform['id']);
				}
			}
		}
		$this->set(compact('forms'));

		$this->render('form');
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Survey->recursive = 0;
		$this->set('surveys', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Survey->exists($id)) {
			throw new NotFoundException(__('Invalid survey'));
		}
		$options = array('conditions' => array('Survey.' . $this->Survey->primaryKey => $id));
		$this->set('survey', $this->Survey->find('first', $options));

		
		$this->Survey->contain(array('SurveyForm'=>array('conditions'=>array('SurveyForm.parent_id <'=>1),'order'=>array('SurveyForm.sequence_no'=>'ASC'),'ChildSurveyForm')));
		$survey = $this->Survey->read(null,$id);		
		$this->set(compact('survey'));
		$steps = array();
		foreach ($survey['SurveyForm'] as $key => $form) {
			$forms[$key] = $this->Survey->getForm($form['id']);
			if(count($form['ChildSurveyForm']) > 0){
				foreach ($form['ChildSurveyForm'] as $subform) {
					$forms[$key]['forms'][] = $this->Survey->getForm($subform['id']);
				}
			}
		}
		$this->set(compact('forms'));
		

		$this->set('statistics', $this->Survey->getStatistics($id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Survey->create();
			if ($this->Survey->save($this->request->data)) {
				$this->Session->setFlash(__('The survey has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The survey could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Survey->exists($id)) {
			throw new NotFoundException(__('Invalid survey'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Survey->save($this->request->data)) {
				$this->Session->setFlash(__('The survey has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The survey could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Survey.' . $this->Survey->primaryKey => $id));
			$this->request->data = $this->Survey->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Survey->id = $id;
		if (!$this->Survey->exists()) {
			throw new NotFoundException(__('Invalid survey'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Survey->delete()) {
			$this->Session->setFlash(__('The survey has been deleted.'));
		} else {
			$this->Session->setFlash(__('The survey could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
