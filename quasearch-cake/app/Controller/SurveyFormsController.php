<?php
App::uses('AppController', 'Controller');
/**
 * SurveyForms Controller
 *
 * @property SurveyForm $SurveyForm
 * @property PaginatorComponent $Paginator
 */
class SurveyFormsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->SurveyForm->recursive = 0;
		//$this->Paginator->settings['order'] = array('SurveyForm.sequence_no'=>'ASC');
		$this->set('surveyForms', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->SurveyForm->exists($id)) {
			throw new NotFoundException(__('Invalid survey form'));
		}
		$options = array('conditions' => array('SurveyForm.' . $this->SurveyForm->primaryKey => $id));
		$this->set('surveyForm', $this->SurveyForm->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->SurveyForm->create();
			if ($this->SurveyForm->save($this->request->data)) {
				$this->Session->setFlash(__('The survey form has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The survey form could not be saved. Please, try again.'));
			}
		}
		$surveys = $this->SurveyForm->Survey->find('list');
		$users = $this->SurveyForm->User->find('list');
		$this->set(compact('surveys', 'users'));

		$parentSurveyForms = $this->SurveyForm->listOptions();
		$parentSurveyForms = array_merge(array('0'=>''),$parentSurveyForms);

		$this->set(compact('parentSurveyForms'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->SurveyForm->exists($id)) {
			throw new NotFoundException(__('Invalid survey form'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->SurveyForm->save($this->request->data)) {
				$this->Session->setFlash(__('The survey form has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The survey form could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('SurveyForm.' . $this->SurveyForm->primaryKey => $id));
			$this->request->data = $this->SurveyForm->find('first', $options);
		}
		$surveys = $this->SurveyForm->Survey->find('list');
		$users = $this->SurveyForm->User->find('list');
		$this->set(compact('surveys', 'users'));

		$parentSurveyForms = $this->SurveyForm->ParentSurveyForm->find('list');
		$parentSurveyForms = array_merge(array('0'=>''),$parentSurveyForms);
		$this->set(compact('parentSurveyForms'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->SurveyForm->id = $id;
		if (!$this->SurveyForm->exists()) {
			throw new NotFoundException(__('Invalid survey form'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->SurveyForm->delete()) {
			$this->Session->setFlash(__('The survey form has been deleted.'));
		} else {
			$this->Session->setFlash(__('The survey form could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
