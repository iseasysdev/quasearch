<?php
App::uses('AppController', 'Controller');
/**
 * Submissions Controller
 *
 * @property Submission $Submission
 * @property PaginatorComponent $Paginator
 */
class SubmissionsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Submission->recursive = 0;
		$this->Paginator->settings['Submission']['order'] = array('Submission.id'=>'desc');		
		$this->set('submissions', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Submission->exists($id)) {
			throw new NotFoundException(__('Invalid submission'));
		}
		$this->Submission->contain(array('User'));
		$options = array('conditions' => array('Submission.' . $this->Submission->primaryKey => $id));
		$submission = $this->Submission->find('first', $options); 
		/*
		$img = $submission['Submission']['image_encoding'];
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);
		pr($img);
		var_dump($data);exit();
		*/


		$this->set('submission',$submission);

		$responses = $this->Submission->getResponses($id);
		$this->set(compact('responses'));

		$this->loadModel('Survey');
		$this->Survey->contain(array('SurveyForm'=>array('conditions'=>array('SurveyForm.parent_id <'=>1),'order'=>array('SurveyForm.sequence_no'=>'ASC'),'ChildSurveyForm')));
		$survey = $this->Survey->read(null,$submission['Submission']['survey_id']);		
		$this->set(compact('survey'));
		$steps = array();
		foreach ($survey['SurveyForm'] as $key => $form) {
			$forms[$key] = $this->Survey->getForm($form['id']);
			if(count($form['ChildSurveyForm']) > 0){
				foreach ($form['ChildSurveyForm'] as $subform) {
					$forms[$key]['forms'][] = $this->Survey->getForm($subform['id']);
				}
			}
		}
		$this->set(compact('forms'));

	}

	public function byquestion($question_id){
		$this->loadModel("Question");
		$this->Question->Behaviors->load('Containable', array('autoFields' => false));
		$this->Question->contain(array('SurveyForm'=>array('Survey'),'QuestionAnswer'=>array('conditions'=>array('QuestionAnswer.value !='=>''))));
		$question = $this->Question->read(null,$question_id);
		$this->set(compact('question'));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->loadModel('Post');
		$this->Post->create();
	        $data['data'] = print_r($_FILES,true);
        	$this->Post->save($data);
        	$data['data'] = json_encode($_POST);

 
		if ($this->request->is('post')) {
			//$this->request->data  = stripslashes('{"data":{\"survey_id\":\"1\",\"latitude\":\"10.3010275\",\"longitude\":\"123.8595981\",\"map_api_url\":\"http:\/\/maps.googleapis.com\/maps\/api\/geocode\/json?latlng=10.3010275,123.8595981&sensor=false\",\"image_encoding\":\"\/9j\/4AAQSkZJRgABAQAAAQABAAD\/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB\nAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH\/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEB\nAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH\/wAARCAHgAlgDASIA\nAhEBAxEB\/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL\/8QAtRAAAgEDAwIEAwUFBAQA\nAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3\nODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWm\np6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6\/8QAHwEA\nAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL\/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSEx\nBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElK\nU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3\nuLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6\/9oADAMBAAIRAxEAPwD+Heii\nigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK\nACiiigAooooAKKKKACiiigAooooAKKKYdmeevHr\/AE4oAfRSDGOOn+f8\/wAqWgAooooAKKKKACii\nigAooooAKKKKAGFcn2wPx9u3+ens\/wDz\/n\/P0ooqHC7vft0\/r8gG7RnP0\/T\/AD\/nu6iirSsku1kA\nUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABR\nRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFF\nFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRSHHGemR\n+dAC0U0bc8dfbpj+X+eKdQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUA\nFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAU\nUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRR\nRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFF\nABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUARIm788Y9f8\/\/AKsc\n1LRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAF\nFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUU\nUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRR\nQAUUUUAFFFFABRRRQBH+7\/zmpKKKACiiigAooooAKKKYwyVH1\/z2\/wA9KAH0UgUL07\/r\/T9PypaA\nCiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAK\nKKKACiiigAooooAZvA459P8AP+fyp45H+f8AP+eMUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUA\nFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAU\nUUUAFFFFABRRRQAUUUUAFFFFQoWad9mugBRRRVgFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFA\nBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAIVB9fwpoQD1\/z+H+e2KfRQAgYHpnt\/nj\/\nAD6UtFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRR\nQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFA\nBRSEZGP8\/wCH6flSBADkZ\/z+FADqKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAK\nKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAoo\nooAKKaVB9fTj\/wDVSgBRgf5\/LH+emKAFooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACi\niigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKK\nKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAoooo\nAKKKKACiiigAooooAKKKKACiiigAooooAKKKKTdlf0\/r+kAUUUVKnd2sAUUUVYBRRRQAUUUUAFFF\nFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUU\nAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQA\nUUUUAFFFFABRRRQAUUUUAFFFFABSHZj5uvb+vT\/PpS0UARp3x04\/r\/n+VSUUUAFFFFABRRRQAUUU\nUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQ\nAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFADE6fj\/hQ3Vfr\/AIU+igAooooAKKKKACii\nigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKK\nACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA\nKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAo\noooAKMZGP88c\/pRRQDV018v6\/r7iMDa4H+e\/p\/h9OMZkoooIULNO+zXQKKKKCwooooAKKKKACiii\ngAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKZt\ny2T0449eP8\/\/AFhQA+iiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAGHZ7fh\/9bin\n0UUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRR\nRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFF\nABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUA\nFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAU\nUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRR\nRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFF\nABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUA\nFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAU\nUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRR\nRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFF\nABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUA\nFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAU\nUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRR\nRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFF\nABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUA\nFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAU\nUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRR\nRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFF\nABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUA\nFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAMC4Yn64\/zwP88ez6KKACiiigAooo\noAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKYxwVP1\/z\/AJH0xT6KTV1b0E1d\nW9BAwbp29un9P89qWiilGHK738trCjDld7+W1goooqihnyc9Pf8A+t\/9b8KeMYGOnH5f5\/8ArUUU\nAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQA\nUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABR\nRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFF\nFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUU\nAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQA\nUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABR\nRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFF\nFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUU\nAFFFFABRRRQAUUUUAFFFFABRRRQAU35cnOM+\/wDnH+eKdRQAgxjjp\/n\/AD\/KloooAKKKKACiiigA\nooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKY3Vfr\/AIU+igAooooAKKKK\nACiiigAooooAKKKKACiiigAooppXOPQdf8\/59ulADqKQDBJ9cf5\/z+GMcrQAUUUUAFFFFABRRRQA\nUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABR\nRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUh6H6G\ngBaKYnT8f8KfQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRR\nQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFA\nBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAF\nFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUU\nUUAFFFFABRRTWGRj3\/z\/AJ\/lQA6imhcHI9Mf5\/z9KdQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQA\nUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABR\nRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFF\nFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUU\nAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUzhBjnn\/Pt\/n0oAfRTSuSCO3+f8\/0p\n1ABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUU\nAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQA\nUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABR\nRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFF\nFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB\/9k=\n\",\"answer\": {\"1\":\"\",\"2\":\"\",\"3\":\"\",\"4\":\"\",\"5\":{\"2\":{\"26\":\"No\",\"27\":\"No\"},\"3\":{\"26\":\"No\",\"27\":\"No\"},\"4\":{\"26\":\"No\",\"27\":\"No\"},\"5\":{\"26\":\"No\",\"27\":\"No\"},\"7\":{\"26\":\"No\",\"27\":\"No\"},\"8\":{\"26\":\"No\",\"27\":\"No\"},\"9\":{\"26\":\"No\",\"27\":\"No\"},\"10\":{\"26\":\"No\",\"27\":\"No\"},\"11\":{\"26\":\"No\",\"27\":\"No\"},\"12\":{\"26\":\"No\",\"27\":\"No\"},\"13\":{\"26\":\"No\",\"27\":\"No\"},\"15\":{\"26\":\"No\",\"27\":\"No\"}},\"6\":\"Very Good\",\"7\":\"Always\",\"8\":\"Always\",\"9\":\"Strongly Agree\",\"10\":\"Always\",\"11\":\"Always\",\"12\":\"Always\",\"13\":\"Strongly Agree\",\"14\":\"Strongly Agree\",\"15\":\"Always\",\"16\":\"Strongly Agree\",\"17\":\"Very important\",\"18\":\"High Impact\",\"19\":\"Much better than expected\",\"20\":{\"16\":{\"21\":\"No\",\"22\":\"No\",\"23\":\"No\",\"24\":\"No\",\"25\":\"No\"},\"17\":{\"21\":\"No\",\"22\":\"No\",\"23\":\"No\",\"24\":\"No\",\"25\":\"No\"}},\"21\":\"Very Good\",\"22\":\"\",\"23\":\"\",\"24\":\"\"}}}');
		 	//$this->request->data  = ('{"data":"{\"survey_id\":\"1\",\"latitude\":\"10.3178458\",\"longitude\":\"123.9070905\",\"map_api_url\":\"http:\/\/maps.googleapis.com\/maps\/api\/geocode\/json?latlng=10.3178458,123.9070905&sensor=false\",\"image_encoding\":\"null\",\"answer\": {\"1\":\"mike\",\"2\":\"google\",\"3\":\"ayala\",\"4\":\"hmmm\",\"5\":{\"2\":{\"26\":\"No\",\"27\":\"No\"},\"3\":{\"26\":\"No\",\"27\":\"No\"},\"4\":{\"26\":\"No\",\"27\":\"No\"},\"5\":{\"26\":\"No\",\"27\":\"No\"},\"7\":{\"26\":\"No\",\"27\":\"No\"},\"8\":{\"26\":\"No\",\"27\":\"No\"},\"9\":{\"26\":\"No\",\"27\":\"No\"},\"10\":{\"26\":\"No\",\"27\":\"No\"},\"11\":{\"26\":\"No\",\"27\":\"No\"},\"12\":{\"26\":\"No\",\"27\":\"No\"},\"13\":{\"26\":\"No\",\"27\":\"No\"},\"15\":{\"26\":\"No\",\"27\":\"No\"}},\"6\":\"Very Good\",\"7\":\"Always\",\"8\":\"Always\",\"9\":\"Strongly Agree\",\"10\":\"Always\",\"11\":\"Always\",\"12\":\"Always\",\"13\":\"Strongly Agree\",\"14\":\"Strongly Agree\",\"15\":\"Always\",\"16\":\"Strongly Agree\",\"17\":\"Very important\",\"18\":\"High Impact\",\"19\":\"Much better than expected\",\"20\":{\"16\":{\"21\":\"No\",\"22\":\"No\",\"23\":\"No\",\"24\":\"No\",\"25\":\"No\"},\"17\":{\"21\":\"No\",\"22\":\"No\",\"23\":\"No\",\"24\":\"No\",\"25\":\"No\"}},\"21\":\"Very Good\",\"22\":\"\",\"23\":\"\",\"24\":\"\"}}"}');
		 	//$data['data'] = '{"data":"{\"survey_id\":\"1\",\"latitude\":\"10.3125791\",\"longitude\":\"123.9113333\",\"map_api_url\":\"http:\/\/maps.googleapis.com\/maps\/api\/geocode\/json?latlng=10.3125791,123.9113333&sensor=false\",\"answer\": {\"1\":\"mike\",\"2\":\"\",\"3\":\"\",\"4\":\"\",\"5\":{\"2\":{\"26\":\"No\",\"27\":\"No\"},\"3\":{\"26\":\"No\",\"27\":\"No\"},\"4\":{\"26\":\"No\",\"27\":\"No\"},\"5\":{\"26\":\"No\",\"27\":\"No\"},\"7\":{\"26\":\"No\",\"27\":\"No\"},\"8\":{\"26\":\"No\",\"27\":\"No\"},\"9\":{\"26\":\"No\",\"27\":\"No\"},\"10\":{\"26\":\"No\",\"27\":\"No\"},\"11\":{\"26\":\"No\",\"27\":\"No\"},\"12\":{\"26\":\"No\",\"27\":\"No\"},\"13\":{\"26\":\"No\",\"27\":\"No\"},\"15\":{\"26\":\"No\",\"27\":\"No\"}},\"6\":\"Very Good\",\"7\":\"Always\",\"8\":\"Always\",\"9\":\"Strongly Agree\",\"10\":\"Always\",\"11\":\"Always\",\"12\":\"Always\",\"13\":\"Strongly Agree\",\"14\":\"Strongly Agree\",\"15\":\"Always\",\"16\":\"Strongly Agree\",\"17\":\"Very important\",\"18\":\"High Impact\",\"19\":\"Much better than expected\",\"20\":{\"16\":{\"21\":\"No\",\"22\":\"No\",\"23\":\"No\",\"24\":\"No\",\"25\":\"No\"},\"17\":{\"21\":\"No\",\"22\":\"No\",\"23\":\"No\",\"24\":\"No\",\"25\":\"No\"}},\"21\":\"Very Good\",\"22\":\"\",\"23\":\"\",\"24\":\"\"}}"}';
			
			$this->Submission->create();
            if($decoded_data = @json_decode($data['data'],true)){
            		if(! is_array($decoded_data['data'])){
            			$decoded_data = @json_decode($decoded_data['data'],true);	
            		}
                    //isJSON                    
                    $this->request->data = $decoded_data;
            }
            $submission['survey_id'] = $this->request->data['survey_id'];
            $submission['user_id'] = 1;//$this->Auth->user('id');//$this->request->data['survey_id']l
            $submission['raw_data'] = json_encode($this->request->data);
            $submission['latitude'] = $this->request->data['latitude'];
            $submission['enumerator'] = $this->request->data['enumerator'];
            $submission['longitude'] = $this->request->data['longitude'];
            $submission['map_api_url'] = $this->request->data['map_api_url'];
            //$submission['image_encoding'] = $this->request->data['image_encoding'];
            $submission['status'] = 'active';



			$allowedExts = array("gif", "jpeg", "jpg", "png");
			$temp = explode(".", $_FILES["image"]["name"]);
			$extension = end($temp); 
			if ($this->Submission->save($submission)) {
				//create directory
				//Image
				$path = WWW_ROOT.'files/'.$this->Submission->id;
				mkdir($path);
				move_uploaded_file($_FILES["image"]["tmp_name"],$path."/". $_FILES["image"]["name"]);
				$this->Submission->saveField('image_encoding',$_FILES["image"]["name"]);
				//AUDIO
				foreach ($_FILES["audio"]['error'] as $question_id => $error) {
				        if ($error == UPLOAD_ERR_OK) {
			            	$tmp_name = $_FILES["audio"]["tmp_name"][$question_id];
			        	    $name = $_FILES["audio"]["name"][$question_id];
			            	//move_uploaded_file($tmp_name, "$uploads_dir/$name");
			            	move_uploaded_file($tmp_name,$path."/". $name);
			            	$this->Submission->QuestionAnswer->updateAll(
			            		array('audio'=>"'{$name}'"),
			            		array('submission_id'=>$this->Submission->id,'question_id'=>$question_id)
			            	);
			            
			        	}
			    	}

				$this->Session->setFlash(__('The submission has been saved.'));
				exit('The submission has been saved.');
				//return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The submission could not be saved. Please, try again.'));exit('The submission could not be saved. Please, try again.');
			}

		}
		$users = $this->Submission->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Submission->exists($id)) {
			throw new NotFoundException(__('Invalid submission'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Submission->save($this->request->data)) {
				$this->Session->setFlash(__('The submission has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The submission could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Submission.' . $this->Submission->primaryKey => $id));
			$this->request->data = $this->Submission->find('first', $options);
		}
		$users = $this->Submission->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Submission->id = $id;
		if (!$this->Submission->exists()) {
			throw new NotFoundException(__('Invalid submission'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Submission->delete()) {
			$this->Session->setFlash(__('The submission has been deleted.'));
		} else {
			$this->Session->setFlash(__('The submission could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
