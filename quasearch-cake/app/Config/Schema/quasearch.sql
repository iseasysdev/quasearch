-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 15, 2014 at 02:09 AM
-- Server version: 5.5.36
-- PHP Version: 5.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quasearch`
--

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

DROP TABLE IF EXISTS `acos`;
CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=316 ;

--
-- Dumping data for table `acos`
--

INSERT INTO `acos` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(302, NULL, NULL, NULL, '', 615, 630),
(303, 302, NULL, NULL, 'Teams', 616, 629),
(304, 303, NULL, NULL, 'index', 617, 618),
(305, 303, NULL, NULL, 'view', 619, 620),
(306, 303, NULL, NULL, 'add', 621, 622),
(307, 303, NULL, NULL, 'edit', 623, 624),
(308, 303, NULL, NULL, 'delete', 625, 626),
(309, 303, NULL, NULL, 'currentUser', 627, 628),
(310, 226, NULL, NULL, 'login', 469, 470),
(311, 226, NULL, NULL, 'logout', 471, 472),
(312, 226, NULL, NULL, 'index', 473, 474),
(313, 226, NULL, NULL, 'view', 475, 476),
(314, 226, NULL, NULL, 'add', 477, 478),
(315, 226, NULL, NULL, 'edit', 479, 480);

-- --------------------------------------------------------

--
-- Table structure for table `aros`
--

DROP TABLE IF EXISTS `aros`;
CREATE TABLE IF NOT EXISTS `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, 'Group', 1, NULL, 1, 6),
(2, NULL, 'Group', 2, NULL, 7, 10),
(3, NULL, 'Group', 3, NULL, 11, 16),
(4, 2, 'User', 2, NULL, 8, 9),
(5, 3, 'User', 3, NULL, 12, 13),
(6, NULL, 'Group', 4, NULL, 17, 20),
(7, NULL, 'Group', 5, NULL, 21, 24),
(8, NULL, 'Group', 6, NULL, 25, 28),
(9, 1, 'User', 1, NULL, 2, 3),
(10, 3, 'User', 4, NULL, 14, 15),
(11, 6, 'User', 5, NULL, 18, 19),
(12, 7, 'User', 6, NULL, 22, 23),
(13, 8, 'User', 7, NULL, 26, 27),
(14, 1, 'Member', 1, NULL, 4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `aros_acos`
--

DROP TABLE IF EXISTS `aros_acos`;
CREATE TABLE IF NOT EXISTS `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `audits`
--

DROP TABLE IF EXISTS `audits`;
CREATE TABLE IF NOT EXISTS `audits` (
  `id` varchar(36) NOT NULL,
  `event` varchar(255) NOT NULL,
  `model` varchar(255) NOT NULL,
  `entity_id` varchar(36) NOT NULL,
  `json_object` text NOT NULL,
  `description` text,
  `source_id` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `audits`
--

INSERT INTO `audits` (`id`, `event`, `model`, `entity_id`, `json_object`, `description`, `source_id`, `created`) VALUES
('538c555c-57a4-47dc-bae7-14f86ff0fe6a', 'CREATE', 'Payment', '5', '{"Payment":{"id":"5","user_id":"1","invoice_id":"5","billing_id":"0","type":"Cash","payment_date":"2014-06-02","amount":"150.00","reference":"1235","check_number":null,"bank_name":null,"bank_branch":null,"notes":"","created":"2014-06-02 12:43:40","modified":"2014-06-02 12:43:40"}}', 'admin', '1', '2014-06-02 12:43:40'),
('538c555d-f4c4-4f25-b4de-14f86ff0fe6a', 'CREATE', 'Collection', '3', '{"Collection":{"id":"3","user_id":"0","category_id":"4","invoice_id":"5","homeowner_id":"1","collection_date":"2014-06-02","particular":"Vehicle Sticker ","amount":"150.00","time_from":"0000-00-00 00:00:00","time_to":"0000-00-00 00:00:00","created":"2014-06-02 12:43:40","modified":"2014-06-02 12:43:40","status":""}}', 'admin', '1', '2014-06-02 12:43:41'),
('538c55aa-7b44-4e8a-afe2-14f86ff0fe6a', 'CREATE', 'Payment', '6', '{"Payment":{"id":"6","user_id":"1","invoice_id":"0","billing_id":"0","type":"Cash","payment_date":"2014-06-02","amount":"3000.00","reference":"1236","check_number":null,"bank_name":null,"bank_branch":null,"notes":"","created":"2014-06-02 12:44:58","modified":"2014-06-02 12:44:58"}}', 'admin', '1', '2014-06-02 12:44:58'),
('53955a9d-a94c-4e8c-ae70-12506ff0fe6a', 'CREATE', 'Payment', '7', '{"Payment":{"id":"7","user_id":"1","invoice_id":"0","billing_id":"0","type":"Cash","payment_date":"2014-06-09","amount":"1334.04","reference":"1237","check_number":null,"bank_name":null,"bank_branch":null,"notes":"","created":"2014-06-09 08:56:29","modified":"2014-06-09 08:56:29"}}', 'admin', '1', '2014-06-09 08:56:29'),
('539563b2-4a3c-44b6-a199-12506ff0fe6a', 'CREATE', 'Discount', '1', '{"Discount":{"id":"1","homeowner_id":"1","user_id":"0","discount_date":"0000-00-00","particular":"Early Payment Discount","amount":"228.39","created":"2014-06-09 09:35:14"}}', 'admin', '1', '2014-06-09 09:35:14'),
('539563b2-d59c-462e-a290-12506ff0fe6a', 'CREATE', 'Payment', '8', '{"Payment":{"id":"8","user_id":"1","invoice_id":"0","billing_id":"10","type":"Cash","payment_date":"2014-06-09","amount":"3188.37","reference":"1238","check_number":null,"bank_name":null,"bank_branch":null,"notes":"","created":"2014-06-09 09:35:14","modified":"2014-06-09 09:35:14"}}', 'admin', '1', '2014-06-09 09:35:14'),
('539563c9-f19c-433c-adda-12506ff0fe6a', 'CREATE', 'Discount', '2', '{"Discount":{"id":"2","homeowner_id":"1","user_id":"0","discount_date":"0000-00-00","particular":"Early Payment Discount","amount":"228.39","created":"2014-06-09 09:35:37"}}', 'admin', '1', '2014-06-09 09:35:37'),
('5395649b-0dbc-4ec9-8d2b-12506ff0fe6a', 'CREATE', 'Discount', '3', '{"Discount":{"id":"3","homeowner_id":"1","user_id":"1","discount_date":"0000-00-00","particular":"Early Payment Discount","amount":"228.39","created":"2014-06-09 09:39:07"}}', 'admin', '1', '2014-06-09 09:39:07'),
('539564db-dd1c-481b-8744-12506ff0fe6a', 'CREATE', 'Discount', '4', '{"Discount":{"id":"4","homeowner_id":"1","user_id":"1","discount_date":"0000-00-00","particular":"Early Payment Discount","amount":"228.39","created":"2014-06-09 09:40:11"}}', 'admin', '1', '2014-06-09 09:40:11'),
('539564fd-801c-4a4c-95bc-12506ff0fe6a', 'CREATE', 'Discount', '5', '{"Discount":{"id":"5","homeowner_id":"1","user_id":"1","discount_date":"0000-00-00","particular":"Early Payment Discount","amount":"228.39","created":"2014-06-09 09:40:45"}}', 'admin', '1', '2014-06-09 09:40:45'),
('53956513-507c-4784-9c0b-12506ff0fe6a', 'CREATE', 'Discount', '6', '{"Discount":{"id":"6","homeowner_id":"1","user_id":"1","discount_date":"0000-00-00","particular":"Early Payment Discount","amount":"228.39","created":"2014-06-09 09:41:07"}}', 'admin', '1', '2014-06-09 09:41:07'),
('539565a3-337c-4a96-b6b3-12506ff0fe6a', 'CREATE', 'Discount', '7', '{"Discount":{"id":"7","homeowner_id":"1","user_id":"1","discount_date":"0000-00-00","particular":"Early Payment Discount","amount":"228.39","created":"2014-06-09 09:43:31"}}', 'admin', '1', '2014-06-09 09:43:31'),
('539565a3-cc9c-4908-b75c-12506ff0fe6a', 'CREATE', 'Payment', '9', '{"Payment":{"id":"9","user_id":"1","invoice_id":"0","billing_id":"11","type":"Cash","payment_date":"2014-06-09","amount":"3188.37","reference":"1239","check_number":null,"bank_name":null,"bank_branch":null,"notes":"","created":"2014-06-09 09:43:31","modified":"2014-06-09 09:43:31"}}', 'admin', '1', '2014-06-09 09:43:31'),
('53956694-5edc-415a-bc43-12506ff0fe6a', 'CREATE', 'Setting', '5', '{"Setting":{"id":"5","name":"Minimum Surchargeable Balance","description":"Minimum Surchargeable Balance","key":"surchargeable","value":"5000","created":"2014-06-09 09:47:32","modified":"2014-06-09 09:47:32"}}', 'admin', '1', '2014-06-09 09:47:32'),
('53956717-3f7c-4866-94c9-12506ff0fe6a', 'EDIT', 'Setting', '5', '{"Setting":{"id":"5","name":"Minimum Surchargeable Balance","description":"Minimum Surchargeable Balance","key":"surchargeable","value":"500","created":"2014-06-09 09:47:32","modified":"2014-06-09 09:49:43"}}', 'admin', '1', '2014-06-09 09:49:43'),
('5395675e-295c-4db2-b3a0-12506ff0fe6a', 'CREATE', 'Payment', '10', '{"Payment":{"id":"10","user_id":"1","invoice_id":"0","billing_id":"13","type":"Cash","payment_date":"2014-06-09","amount":"5000.00","reference":"1240","check_number":null,"bank_name":null,"bank_branch":null,"notes":"","created":"2014-06-09 09:50:54","modified":"2014-06-09 09:50:54"}}', 'admin', '1', '2014-06-09 09:50:54'),
('5395675e-479c-4557-aa55-12506ff0fe6a', 'CREATE', 'Discount', '8', '{"Discount":{"id":"8","homeowner_id":"1","user_id":"1","discount_date":"0000-00-00","particular":"Early Payment Discount","amount":"228.39","created":"2014-06-09 09:50:54"}}', 'admin', '1', '2014-06-09 09:50:54'),
('53956a5e-3cdc-4f95-94f9-12506ff0fe6a', 'CREATE', 'Payment', '11', '{"Payment":{"id":"11","user_id":"1","invoice_id":"0","billing_id":"15","type":"Cash","payment_date":"2014-06-09","amount":"423.28","reference":"1241","check_number":null,"bank_name":null,"bank_branch":null,"notes":"","created":"2014-06-09 10:03:42","modified":"2014-06-09 10:03:42"}}', 'admin', '1', '2014-06-09 10:03:42'),
('53956a5e-54bc-412a-b5da-12506ff0fe6a', 'CREATE', 'Discount', '9', '{"Discount":{"id":"9","homeowner_id":"1","user_id":"1","discount_date":"0000-00-00","particular":"Early Payment Discount","amount":"47.03","created":"2014-06-09 10:03:42"}}', 'admin', '1', '2014-06-09 10:03:42');

-- --------------------------------------------------------

--
-- Table structure for table `audit_deltas`
--

DROP TABLE IF EXISTS `audit_deltas`;
CREATE TABLE IF NOT EXISTS `audit_deltas` (
  `id` varchar(36) NOT NULL,
  `audit_id` varchar(36) NOT NULL,
  `property_name` varchar(255) NOT NULL,
  `old_value` text,
  `new_value` text,
  PRIMARY KEY (`id`),
  KEY `audit_id` (`audit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `audit_deltas`
--

INSERT INTO `audit_deltas` (`id`, `audit_id`, `property_name`, `old_value`, `new_value`) VALUES
('53956717-f8fc-499d-a978-12506ff0fe6a', '53956717-3f7c-4866-94c9-12506ff0fe6a', 'value', '5000', '500');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `created` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `created`, `status`) VALUES
(1, 'SuperAdministrator', '2014-05-06 20:41:24', 'active'),
(2, 'Administrator', '2014-05-06 20:41:47', 'active'),
(3, 'Staff', '2014-05-06 20:42:02', 'active'),
(4, 'SalesManager', '2014-07-01 12:19:42', 'active'),
(5, 'Broker', '2014-07-01 12:19:55', 'active'),
(6, 'Agent', '2014-07-01 12:20:29', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `login_logs`
--

DROP TABLE IF EXISTS `login_logs`;
CREATE TABLE IF NOT EXISTS `login_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `ip_address` varchar(40) NOT NULL,
  `result` text NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `login_logs`
--

INSERT INTO `login_logs` (`id`, `user_id`, `username`, `ip_address`, `result`, `created`) VALUES
(1, 0, 'kbitoon', '127.0.0.1', 'Login Failed: Unknown Username', '2014-06-23 04:55:39'),
(2, 0, 'kbitoon', '127.0.0.1', 'Login Failed: Unknown Username', '2014-06-23 04:56:51'),
(3, 1, 'admin', '127.0.0.1', 'success', '2014-06-23 04:57:22'),
(4, 1, 'admin', '::1', 'success', '2014-06-28 15:29:13'),
(5, 1, 'admin', '127.0.0.1', 'Login Failed: Invalid Password', '2014-07-01 11:30:47'),
(6, 1, 'admin', '127.0.0.1', 'Login Failed: Invalid Password', '2014-07-01 11:35:31'),
(7, 1, 'admin', '127.0.0.1', 'Login Failed: Invalid Password', '2014-07-01 11:35:40'),
(8, 1, 'admin', '127.0.0.1', 'success', '2014-07-01 11:35:54'),
(9, 1, 'admin', '127.0.0.1', 'success', '2014-07-02 13:03:52'),
(10, 1, 'admin', '127.0.0.1', 'success', '2014-07-04 07:00:24'),
(11, 1, 'admin', '127.0.0.1', 'success', '2014-07-05 07:03:20'),
(12, 1, 'admin', '::1', 'success', '2014-07-06 02:35:59'),
(13, 1, 'admin', '::1', 'success', '2014-07-06 06:36:33'),
(14, 1, 'admin', '::1', 'success', '2014-07-06 11:51:59'),
(15, 1, 'admin', '::1', 'success', '2014-07-06 15:53:07'),
(16, 1, 'admin', '::1', 'success', '2014-07-08 15:16:03'),
(17, 1, 'admin', '127.0.0.1', 'success', '2014-07-09 09:15:33'),
(18, 1, 'admin', '127.0.0.1', 'success', '2014-07-11 11:19:32');

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

DROP TABLE IF EXISTS `options`;
CREATE TABLE IF NOT EXISTS `options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `title`, `created`, `modified`, `status`) VALUES
(1, 'High Quality Ratings', '2014-07-05 07:17:24', '2014-07-09 05:12:39', 'active'),
(2, 'Performance Ratings', '2014-07-06 02:40:49', '2014-07-06 02:40:49', 'active'),
(3, 'Occurence Ratings', '2014-07-06 02:43:34', '2014-07-06 02:43:34', 'active'),
(4, 'Satisfaction Ratings', '2014-07-06 02:45:15', '2014-07-06 02:45:15', 'active'),
(5, 'Agreement Options', '2014-07-06 03:22:47', '2014-07-06 03:23:03', 'active'),
(6, 'Training Attended fields', '2014-07-06 06:53:12', '2014-07-06 06:53:12', 'active'),
(7, 'Good Quality Ratings', '2014-07-09 05:12:51', '2014-07-09 05:12:51', 'active'),
(8, 'Usual Occurence Ratings', '2014-07-09 05:17:44', '2014-07-09 05:17:44', 'active'),
(9, 'Decide Agreement Options', '2014-07-09 05:23:37', '2014-07-09 05:23:37', 'active'),
(10, 'Importance Options', '2014-07-09 05:26:12', '2014-07-09 05:26:12', 'active'),
(11, 'Impact Options', '2014-07-09 05:28:18', '2014-07-09 05:28:18', 'active'),
(12, 'Expected Options', '2014-07-09 05:30:17', '2014-07-09 05:30:17', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `option_items`
--

DROP TABLE IF EXISTS `option_items`;
CREATE TABLE IF NOT EXISTS `option_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=58 ;

--
-- Dumping data for table `option_items`
--

INSERT INTO `option_items` (`id`, `option_id`, `value`, `created`, `modified`, `status`) VALUES
(1, 1, 'Very High', '2014-07-05 07:24:11', '2014-07-05 07:24:11', 'active'),
(2, 1, 'A little above average', '2014-07-05 07:24:30', '2014-07-05 07:24:30', 'active'),
(3, 1, 'Average', '2014-07-05 07:24:39', '2014-07-05 07:24:39', 'active'),
(4, 1, 'A little below average', '2014-07-05 07:24:54', '2014-07-05 07:24:54', 'active'),
(5, 1, 'Very low', '2014-07-05 07:25:06', '2014-07-05 07:25:06', 'active'),
(6, 2, 'Much better than expected', '2014-07-06 02:41:47', '2014-07-06 02:41:47', 'active'),
(7, 2, 'Somewhat better than expected', '2014-07-06 02:42:00', '2014-07-06 02:42:00', 'active'),
(8, 2, 'Same as expected', '2014-07-06 02:42:12', '2014-07-06 02:42:12', 'active'),
(9, 2, 'Somewhat worse than expected', '2014-07-06 02:42:35', '2014-07-06 02:42:35', 'active'),
(10, 2, 'Much worse than expected', '2014-07-06 02:42:51', '2014-07-06 02:42:51', 'active'),
(11, 3, 'Always', '2014-07-06 02:44:01', '2014-07-06 02:44:01', 'active'),
(12, 3, 'Very Often', '2014-07-06 02:44:11', '2014-07-06 02:44:11', 'active'),
(13, 3, 'Sometimes', '2014-07-06 02:44:18', '2014-07-06 02:44:18', 'active'),
(14, 3, 'Rarely', '2014-07-06 02:44:26', '2014-07-06 02:44:26', 'active'),
(15, 3, 'Never', '2014-07-06 02:44:43', '2014-07-06 02:44:43', 'active'),
(16, 4, 'Very satisfied', '2014-07-06 02:45:40', '2014-07-06 02:45:40', 'active'),
(17, 4, 'Somewhat satisfied', '2014-07-06 02:45:51', '2014-07-06 02:45:51', 'active'),
(18, 4, 'Undecided', '2014-07-06 02:45:59', '2014-07-06 02:45:59', 'active'),
(19, 4, 'Somewhat dissatisfied', '2014-07-06 02:46:12', '2014-07-06 02:46:12', 'active'),
(20, 4, 'Very dissatisfied', '2014-07-06 02:46:24', '2014-07-06 02:46:24', 'active'),
(21, 5, 'Strongly Agree', '2014-07-06 03:26:31', '2014-07-06 03:26:31', 'active'),
(22, 5, 'Agree', '2014-07-06 03:26:45', '2014-07-06 03:26:45', 'active'),
(23, 5, 'Neutral', '2014-07-06 03:26:55', '2014-07-06 03:26:55', 'active'),
(24, 5, 'Disagree', '2014-07-06 03:27:06', '2014-07-06 03:27:06', 'active'),
(25, 5, 'Strongly Disagree', '2014-07-06 03:27:17', '2014-07-06 03:27:17', 'active'),
(26, 6, 'Relevant VisayasHealth products/services you are AWARE of', '2014-07-06 06:53:52', '2014-07-06 06:53:52', 'active'),
(27, 6, 'VisayasHealth products and services USED in the previous year', '2014-07-06 06:54:06', '2014-07-06 06:54:06', 'active'),
(28, 7, 'Very Good', '2014-07-09 05:14:14', '2014-07-09 05:14:14', 'active'),
(29, 7, 'Good', '2014-07-09 05:14:17', '2014-07-09 05:14:17', 'active'),
(30, 7, 'Barely Acceptable', '2014-07-09 05:14:19', '2014-07-09 05:14:19', 'active'),
(31, 7, 'Poor', '2014-07-09 05:14:21', '2014-07-09 05:14:21', 'active'),
(32, 7, 'Very Poor', '2014-07-09 05:14:23', '2014-07-09 05:14:23', 'active'),
(33, 8, 'Always', '2014-07-09 05:18:21', '2014-07-09 05:18:21', 'active'),
(34, 8, 'Usually', '2014-07-09 05:18:26', '2014-07-09 05:18:26', 'active'),
(35, 8, 'About half the time', '2014-07-09 05:18:34', '2014-07-09 05:18:34', 'active'),
(36, 8, 'Seldom', '2014-07-09 05:18:41', '2014-07-09 05:18:41', 'active'),
(37, 8, 'Never', '2014-07-09 05:18:46', '2014-07-09 05:18:46', 'active'),
(38, 9, 'Strongly Agree', '2014-07-09 05:24:34', '2014-07-09 05:24:34', 'active'),
(39, 9, 'Agree', '2014-07-09 05:24:40', '2014-07-09 05:24:40', 'active'),
(40, 9, 'Undecided', '2014-07-09 05:24:43', '2014-07-09 05:24:43', 'active'),
(41, 9, 'Disagree', '2014-07-09 05:24:46', '2014-07-09 05:24:46', 'active'),
(42, 9, 'Strongly Disagree', '2014-07-09 05:24:49', '2014-07-09 05:24:49', 'active'),
(43, 10, 'Very important', '2014-07-09 05:27:12', '2014-07-09 05:27:12', 'active'),
(44, 10, 'Important', '2014-07-09 05:27:14', '2014-07-09 05:27:14', 'active'),
(45, 10, 'Moderately Important', '2014-07-09 05:27:17', '2014-07-09 05:27:17', 'active'),
(46, 10, 'Of Little Importance', '2014-07-09 05:27:20', '2014-07-09 05:27:20', 'active'),
(47, 10, 'Unimportant', '2014-07-09 05:27:22', '2014-07-09 05:27:22', 'active'),
(48, 11, 'High Impact', '2014-07-09 05:29:11', '2014-07-09 05:29:11', 'active'),
(49, 11, 'Somewhat High Impact', '2014-07-09 05:29:13', '2014-07-09 05:29:13', 'active'),
(50, 11, 'Medium Impact', '2014-07-09 05:29:16', '2014-07-09 05:29:16', 'active'),
(51, 11, 'Somewhat Low', '2014-07-09 05:29:20', '2014-07-09 05:29:20', 'active'),
(52, 11, 'Low Impact', '2014-07-09 05:29:22', '2014-07-09 05:29:22', 'active'),
(53, 12, 'Much better than expected', '2014-07-09 05:30:34', '2014-07-09 05:30:34', 'active'),
(54, 12, 'Somewhat better than expected', '2014-07-09 05:30:46', '2014-07-09 05:30:46', 'active'),
(55, 12, 'Same as expected', '2014-07-09 05:30:57', '2014-07-09 05:30:57', 'active'),
(56, 12, 'Somewhat worse than expected', '2014-07-09 05:31:11', '2014-07-09 05:31:11', 'active'),
(57, 12, 'Much worse than expected', '2014-07-09 05:31:22', '2014-07-09 05:31:22', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `data`) VALUES
(15, 'Array\n(\n    [image] => Array\n        (\n            [name] => janet_20140814_220830.jpg\n            [type] => image/jpeg\n            [tmp_name] => /tmp/phpSUCQhX\n            [error] => 0\n            [size] => 251382\n        )\n\n    [audio] => Array\n        (\n            [name] => Array\n                (\n                    [24] => 20140814_220822.mp4\n                    [23] => 20140814_220815.mp4\n                    [22] => 20140814_220805.mp4\n                )\n\n            [type] => Array\n                (\n                    [24] => audio/mp4\n                    [23] => audio/mp4\n                    [22] => audio/mp4\n                )\n\n            [tmp_name] => Array\n                (\n                    [24] => /tmp/phpfLRJjf\n                    [23] => /tmp/phpRB4Dlx\n                    [22] => /tmp/phpQJMynP\n                )\n\n            [error] => Array\n                (\n                    [24] => 0\n                    [23] => 0\n                    [22] => 0\n                )\n\n            [size] => Array\n                (\n                    [24] => 10293\n                    [23] => 8335\n                    [22] => 13005\n                )\n\n        )\n\n)\n');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `survey_form_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_sequence` int(11) DEFAULT NULL,
  `weight` int(10) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `survey_form_id`, `option_id`, `type`, `question`, `total_sequence`, `weight`, `created`, `modified`, `status`) VALUES
(1, 1, 0, 'Short Answer', 'Name of Respondent (optional)', NULL, 0, '2014-07-08 16:30:19', '2014-07-08 16:30:19', 'active'),
(2, 1, 0, 'Short Answer', 'Name of Organization', NULL, 0, '2014-07-08 16:30:33', '2014-07-08 16:30:33', 'active'),
(3, 1, 0, 'Long Answer', 'Address of Respondent/Organization', NULL, 0, '2014-07-08 16:30:48', '2014-07-08 16:30:48', 'active'),
(4, 1, 0, 'Long Answer', 'Position of Respondent in Organization', NULL, 0, '2014-07-08 16:31:02', '2014-07-08 16:31:02', 'active'),
(5, 3, 6, 'Multiple Items', 'What VisayasHealth Technical Assistance Packages are you aware of and have availed during the previous year (please check appropriate columns)', NULL, 0, '2014-07-08 16:32:34', '2014-07-08 16:32:34', 'active'),
(6, 4, 7, 'Multiple Choice', 'Please rate the quality of technical assistance/support provided by VisayasHealth to your organization in the previous year', NULL, 0, '2014-07-09 05:15:08', '2014-07-09 05:15:08', 'active'),
(7, 4, 8, 'Multiple Choice', 'To what extent has VisayasHealth accommodated your requests for technical assistance?', NULL, 0, '2014-07-09 05:16:34', '2014-07-20 08:48:31', 'active'),
(8, 4, 8, 'Multiple Choice', 'VisayasHealth has responded to your organizationâ€™s requests for technical assistance in a timely manner?', NULL, 0, '2014-07-09 05:16:36', '2014-07-20 08:53:17', 'active'),
(9, 4, 5, 'Multiple Choice', 'VisayasHealth has adequate technical expertise to support your organization.', NULL, 0, '2014-07-09 05:16:38', '2014-07-20 08:56:55', 'active'),
(10, 5, 8, 'Multiple Choice', 'To what extent has VisayasHealth encouraged your organization development of its plans and programs related to your organization?', NULL, 0, '2014-07-09 05:21:29', '2014-07-09 05:21:29', 'active'),
(11, 5, 8, 'Multiple Choice', 'VisayasHealth has responded effectively to your organizationâ€™s suggestions on strategies and proposed activities.', NULL, 0, '2014-07-09 05:21:38', '2014-07-09 05:21:38', 'active'),
(12, 5, 8, 'Multiple Choice', 'Your organization has been actively involved in the discussion of project progress, implementation issues, and identification of corrective measures.', NULL, 0, '2014-07-09 05:21:42', '2014-07-09 05:21:42', 'active'),
(13, 5, 9, 'Multiple Choice', 'The role of your organization and other stakeholders are well defined?', NULL, 0, '2014-07-09 05:21:49', '2014-07-20 08:57:46', 'active'),
(14, 5, 9, 'Multiple Choice', 'VisayasHealth provides adequate information on its plans and programs.', NULL, 0, '2014-07-09 05:21:54', '2014-07-20 08:58:23', 'active'),
(15, 5, 8, 'Multiple Choice', 'Are adequate resources made available by VisayasHealth for strengthening your organizationâ€™s capabilities?', NULL, 0, '2014-07-09 05:21:59', '2014-07-09 05:21:59', 'active'),
(16, 6, 9, 'Multiple Choice', 'After a year of implementation, project assumptions, strategies, and activities related to your organization are still relevant.', NULL, 0, '2014-07-09 05:25:16', '2014-07-09 05:25:16', 'active'),
(17, 6, 10, 'Multiple Choice', 'How would you rate the importance of VisayasHealthâ€™s plans and programs related to your organization?', NULL, 0, '2014-07-09 05:27:53', '2014-07-09 05:27:53', 'active'),
(18, 6, 11, 'Multiple Choice', 'How would you rate the expected impact/benefits of project plans and programs', NULL, 0, '2014-07-09 05:29:49', '2014-07-09 05:29:49', 'active'),
(19, 6, 12, 'Multiple Choice', 'To what extent have plans and programs in the previous year been implemented?', NULL, 0, '2014-07-09 05:31:48', '2014-07-09 05:31:48', 'active'),
(20, 7, 5, 'Multiple Items', 'Project Administration', NULL, 0, '2014-07-09 05:33:10', '2014-07-09 05:33:10', 'active'),
(21, 8, 7, 'Multiple Choice', 'What is your overall assessment of the quality of Technical Assistance provided by VisayasHealth?', NULL, 0, '2014-07-09 05:35:35', '2014-07-09 05:35:35', 'active'),
(22, 8, 0, 'Long Answer', 'What did you like about VisayasHealthâ€™s Technical Assistance to your organization?', NULL, 0, '2014-07-09 05:36:13', '2014-07-09 05:36:13', 'active'),
(23, 8, 0, 'Long Answer', 'What did you not like about VisayasHealthâ€™s Technical Assistance to your organization?', NULL, 0, '2014-07-09 05:36:30', '2014-07-09 05:36:30', 'active'),
(24, 8, 0, 'Long Answer', 'In your opinion, how can VisayasHealth improve its Technical Assistance to your organization?', NULL, 0, '2014-07-09 05:36:56', '2014-07-09 05:36:56', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `question_answers`
--

DROP TABLE IF EXISTS `question_answers`;
CREATE TABLE IF NOT EXISTS `question_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `audio` varchar(255) NOT NULL,
  `created` int(11) NOT NULL,
  `modified` int(11) NOT NULL,
  `stat` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=331 ;

--
-- Dumping data for table `question_answers`
--

INSERT INTO `question_answers` (`id`, `submission_id`, `question_id`, `value`, `audio`, `created`, `modified`, `stat`) VALUES
(1, 1, 1, 'test2', '', 1407906812, 1407906812, 0),
(2, 1, 2, 'hdje', '', 1407906812, 1407906812, 0),
(3, 1, 3, 'heue', '', 1407906812, 1407906812, 0),
(4, 1, 4, 'ueh', '', 1407906812, 1407906812, 0),
(5, 1, 6, 'Very Good', '', 1407906812, 1407906812, 0),
(6, 1, 7, 'Always', '', 1407906812, 1407906812, 0),
(7, 1, 8, 'Always', '', 1407906812, 1407906812, 0),
(8, 1, 9, 'Strongly Agree', '', 1407906812, 1407906812, 0),
(9, 1, 10, 'Always', '', 1407906812, 1407906812, 0),
(10, 1, 11, 'Always', '', 1407906813, 1407906813, 0),
(11, 1, 12, 'Always', '', 1407906813, 1407906813, 0),
(12, 1, 13, 'Strongly Agree', '', 1407906813, 1407906813, 0),
(13, 1, 14, 'Strongly Agree', '', 1407906813, 1407906813, 0),
(14, 1, 15, 'Always', '', 1407906813, 1407906813, 0),
(15, 1, 16, 'Strongly Agree', '', 1407906813, 1407906813, 0),
(16, 1, 17, 'Very important', '', 1407906813, 1407906813, 0),
(17, 1, 18, 'High Impact', '', 1407906813, 1407906813, 0),
(18, 1, 19, 'Much better than expected', '', 1407906813, 1407906813, 0),
(19, 1, 21, 'Very Good', '', 1407906813, 1407906813, 0),
(20, 1, 22, '', '', 1407906813, 1407906813, 0),
(21, 1, 23, '', '', 1407906813, 1407906813, 0),
(22, 1, 24, '', '', 1407906813, 1407906813, 0),
(23, 2, 1, 'test1', '', 1407906814, 1407906814, 0),
(24, 2, 2, 'hdhbr', '', 1407906814, 1407906814, 0),
(25, 2, 3, 'hfhfb', '', 1407906814, 1407906814, 0),
(26, 2, 4, 'hfhfh', '', 1407906814, 1407906814, 0),
(27, 2, 6, 'Very Good', '', 1407906814, 1407906814, 0),
(28, 2, 7, 'Always', '', 1407906814, 1407906814, 0),
(29, 2, 8, 'Always', '', 1407906814, 1407906814, 0),
(30, 2, 9, 'Strongly Agree', '', 1407906814, 1407906814, 0),
(31, 2, 10, 'Always', '', 1407906814, 1407906814, 0),
(32, 2, 11, 'Always', '', 1407906814, 1407906814, 0),
(33, 2, 12, 'Always', '', 1407906814, 1407906814, 0),
(34, 2, 13, 'Strongly Agree', '', 1407906814, 1407906814, 0),
(35, 2, 14, 'Strongly Agree', '', 1407906814, 1407906814, 0),
(36, 2, 15, 'Always', '', 1407906814, 1407906814, 0),
(37, 2, 16, 'Strongly Agree', '', 1407906814, 1407906814, 0),
(38, 2, 17, 'Very important', '', 1407906814, 1407906814, 0),
(39, 2, 18, 'High Impact', '', 1407906814, 1407906814, 0),
(40, 2, 19, 'Much better than expected', '', 1407906814, 1407906814, 0),
(41, 2, 21, 'Very Good', '', 1407906814, 1407906814, 0),
(42, 2, 22, '', '', 1407906814, 1407906814, 0),
(43, 2, 23, '', '', 1407906814, 1407906814, 0),
(44, 2, 24, '', '', 1407906814, 1407906814, 0),
(45, 3, 1, 'test1', '', 1407913616, 1407913616, 0),
(46, 3, 2, 'hfhd', '', 1407913616, 1407913616, 0),
(47, 3, 3, 'hdhdj', '', 1407913616, 1407913616, 0),
(48, 3, 4, 'dhdjdj', '', 1407913616, 1407913616, 0),
(49, 3, 6, 'Very Good', '', 1407913616, 1407913616, 0),
(50, 3, 7, 'Always', '', 1407913616, 1407913616, 0),
(51, 3, 8, 'Always', '', 1407913616, 1407913616, 0),
(52, 3, 9, 'Strongly Agree', '', 1407913616, 1407913616, 0),
(53, 3, 10, 'Always', '', 1407913616, 1407913616, 0),
(54, 3, 11, 'Always', '', 1407913616, 1407913616, 0),
(55, 3, 12, 'Always', '', 1407913616, 1407913616, 0),
(56, 3, 13, 'Strongly Agree', '', 1407913616, 1407913616, 0),
(57, 3, 14, 'Strongly Agree', '', 1407913617, 1407913617, 0),
(58, 3, 15, 'Always', '', 1407913617, 1407913617, 0),
(59, 3, 16, 'Strongly Agree', '', 1407913617, 1407913617, 0),
(60, 3, 17, 'Very important', '', 1407913617, 1407913617, 0),
(61, 3, 18, 'High Impact', '', 1407913617, 1407913617, 0),
(62, 3, 19, 'Much better than expected', '', 1407913617, 1407913617, 0),
(63, 3, 21, 'Very Good', '', 1407913617, 1407913617, 0),
(64, 3, 22, '', '', 1407913617, 1407913617, 0),
(65, 3, 23, '', '', 1407913617, 1407913617, 0),
(66, 3, 24, '', '', 1407913617, 1407913617, 0),
(67, 4, 1, 'test2', '', 1407913617, 1407913617, 0),
(68, 4, 2, 'hsnd', '', 1407913617, 1407913617, 0),
(69, 4, 3, 'dddd', '', 1407913617, 1407913617, 0),
(70, 4, 4, 'dddd', '', 1407913617, 1407913617, 0),
(71, 4, 6, 'Very Good', '', 1407913617, 1407913617, 0),
(72, 4, 7, 'Always', '', 1407913617, 1407913617, 0),
(73, 4, 8, 'Always', '', 1407913617, 1407913617, 0),
(74, 4, 9, 'Strongly Agree', '', 1407913617, 1407913617, 0),
(75, 4, 10, 'Always', '', 1407913617, 1407913617, 0),
(76, 4, 11, 'Always', '', 1407913617, 1407913617, 0),
(77, 4, 12, 'Always', '', 1407913617, 1407913617, 0),
(78, 4, 13, 'Strongly Agree', '', 1407913617, 1407913617, 0),
(79, 4, 14, 'Strongly Agree', '', 1407913617, 1407913617, 0),
(80, 4, 15, 'Always', '', 1407913617, 1407913617, 0),
(81, 4, 16, 'Strongly Agree', '', 1407913617, 1407913617, 0),
(82, 4, 17, 'Very important', '', 1407913617, 1407913617, 0),
(83, 4, 18, 'High Impact', '', 1407913618, 1407913618, 0),
(84, 4, 19, 'Much better than expected', '', 1407913618, 1407913618, 0),
(85, 4, 21, 'Very Good', '', 1407913618, 1407913618, 0),
(86, 5, 1, 'test2', '', 1407913618, 1407913618, 0),
(87, 4, 22, '', '', 1407913618, 1407913618, 0),
(88, 5, 2, 'hfh', '', 1407913618, 1407913618, 0),
(89, 4, 23, '', '', 1407913618, 1407913618, 0),
(90, 5, 3, 'hbu', '', 1407913618, 1407913618, 0),
(91, 4, 24, '', '', 1407913618, 1407913618, 0),
(92, 5, 4, 'hjf', '', 1407913618, 1407913618, 0),
(93, 5, 6, 'Very Good', '', 1407913618, 1407913618, 0),
(94, 5, 7, 'Always', '', 1407913618, 1407913618, 0),
(95, 5, 8, 'Always', '', 1407913618, 1407913618, 0),
(96, 5, 9, 'Strongly Agree', '', 1407913618, 1407913618, 0),
(97, 5, 10, 'Always', '', 1407913618, 1407913618, 0),
(98, 5, 11, 'Always', '', 1407913618, 1407913618, 0),
(99, 5, 12, 'Always', '', 1407913618, 1407913618, 0),
(100, 5, 13, 'Strongly Agree', '', 1407913618, 1407913618, 0),
(101, 5, 14, 'Strongly Agree', '', 1407913618, 1407913618, 0),
(102, 5, 15, 'Always', '', 1407913619, 1407913619, 0),
(103, 5, 16, 'Strongly Agree', '', 1407913619, 1407913619, 0),
(104, 5, 17, 'Very important', '', 1407913619, 1407913619, 0),
(105, 5, 18, 'High Impact', '', 1407913619, 1407913619, 0),
(106, 5, 19, 'Much better than expected', '', 1407913619, 1407913619, 0),
(107, 5, 21, 'Very Good', '', 1407913619, 1407913619, 0),
(108, 6, 1, 'test1', '', 1407913619, 1407913619, 0),
(109, 5, 22, '', '20140813_144543.mp4', 1407913619, 1407913619, 0),
(110, 6, 2, 'qwerty', '', 1407913619, 1407913619, 0),
(111, 5, 23, '', '', 1407913619, 1407913619, 0),
(112, 6, 3, 'qwerty', '', 1407913619, 1407913619, 0),
(113, 5, 24, '', '', 1407913619, 1407913619, 0),
(114, 6, 4, 'qaws', '', 1407913619, 1407913619, 0),
(115, 6, 6, 'Very Good', '', 1407913619, 1407913619, 0),
(116, 6, 7, 'Always', '', 1407913619, 1407913619, 0),
(117, 6, 8, 'Always', '', 1407913619, 1407913619, 0),
(118, 6, 9, 'Strongly Agree', '', 1407913619, 1407913619, 0),
(119, 6, 10, 'Always', '', 1407913619, 1407913619, 0),
(120, 6, 11, 'Always', '', 1407913619, 1407913619, 0),
(121, 6, 12, 'Always', '', 1407913619, 1407913619, 0),
(122, 6, 13, 'Strongly Agree', '', 1407913619, 1407913619, 0),
(123, 6, 14, 'Strongly Agree', '', 1407913619, 1407913619, 0),
(124, 6, 15, 'Always', '', 1407913619, 1407913619, 0),
(125, 6, 16, 'Strongly Agree', '', 1407913619, 1407913619, 0),
(126, 6, 17, 'Very important', '', 1407913619, 1407913619, 0),
(127, 6, 18, 'High Impact', '', 1407913619, 1407913619, 0),
(128, 6, 19, 'Much better than expected', '', 1407913620, 1407913620, 0),
(129, 6, 21, 'Very Good', '', 1407913620, 1407913620, 0),
(130, 6, 22, '', '', 1407913620, 1407913620, 0),
(131, 6, 23, '', '', 1407913620, 1407913620, 0),
(132, 6, 24, '', '', 1407913620, 1407913620, 0),
(133, 7, 1, 'test1', '', 1407914368, 1407914368, 0),
(134, 7, 2, 'gdhd', '', 1407914368, 1407914368, 0),
(135, 7, 3, 'dhdhd', '', 1407914368, 1407914368, 0),
(136, 8, 1, 'test2', '', 1407914368, 1407914368, 0),
(137, 7, 4, 'dhdhd', '', 1407914368, 1407914368, 0),
(138, 8, 2, 'bdhdj', '', 1407914368, 1407914368, 0),
(139, 7, 6, 'Very Good', '', 1407914368, 1407914368, 0),
(140, 8, 3, 'dhdh', '', 1407914368, 1407914368, 0),
(141, 7, 7, 'Always', '', 1407914368, 1407914368, 0),
(142, 8, 4, 'dhdjd', '', 1407914369, 1407914369, 0),
(143, 7, 8, 'Always', '', 1407914369, 1407914369, 0),
(144, 8, 6, 'Very Good', '', 1407914369, 1407914369, 0),
(145, 7, 9, 'Strongly Agree', '', 1407914369, 1407914369, 0),
(146, 8, 7, 'Always', '', 1407914369, 1407914369, 0),
(147, 7, 10, 'Always', '', 1407914369, 1407914369, 0),
(148, 8, 8, 'Always', '', 1407914369, 1407914369, 0),
(149, 7, 11, 'Always', '', 1407914369, 1407914369, 0),
(150, 8, 9, 'Strongly Agree', '', 1407914369, 1407914369, 0),
(151, 7, 12, 'Always', '', 1407914369, 1407914369, 0),
(152, 8, 10, 'Always', '', 1407914369, 1407914369, 0),
(153, 7, 13, 'Strongly Agree', '', 1407914369, 1407914369, 0),
(154, 8, 11, 'Always', '', 1407914369, 1407914369, 0),
(155, 7, 14, 'Strongly Agree', '', 1407914369, 1407914369, 0),
(156, 8, 12, 'Always', '', 1407914369, 1407914369, 0),
(157, 7, 15, 'Always', '', 1407914369, 1407914369, 0),
(158, 8, 13, 'Strongly Agree', '', 1407914369, 1407914369, 0),
(159, 7, 16, 'Strongly Agree', '', 1407914369, 1407914369, 0),
(160, 8, 14, 'Strongly Agree', '', 1407914369, 1407914369, 0),
(161, 7, 17, 'Very important', '', 1407914369, 1407914369, 0),
(162, 8, 15, 'Always', '', 1407914369, 1407914369, 0),
(163, 7, 18, 'High Impact', '', 1407914369, 1407914369, 0),
(164, 8, 16, 'Strongly Agree', '', 1407914369, 1407914369, 0),
(165, 7, 19, 'Much better than expected', '', 1407914369, 1407914369, 0),
(166, 8, 17, 'Very important', '', 1407914369, 1407914369, 0),
(167, 7, 21, 'Very Good', '', 1407914369, 1407914369, 0),
(168, 8, 18, 'High Impact', '', 1407914369, 1407914369, 0),
(169, 7, 22, '', '', 1407914369, 1407914369, 0),
(170, 8, 19, 'Much better than expected', '', 1407914369, 1407914369, 0),
(171, 7, 23, '', '', 1407914370, 1407914370, 0),
(172, 8, 21, 'Very Good', '', 1407914370, 1407914370, 0),
(173, 7, 24, '', '', 1407914370, 1407914370, 0),
(174, 8, 22, '', '', 1407914370, 1407914370, 0),
(175, 8, 23, '', '', 1407914370, 1407914370, 0),
(176, 8, 24, '', '', 1407914370, 1407914370, 0),
(177, 9, 1, 'tst2', '', 1407914831, 1407914831, 0),
(178, 9, 2, 'rhdjd', '', 1407914831, 1407914831, 0),
(179, 9, 3, 'dhejr', '', 1407914831, 1407914831, 0),
(180, 10, 1, 'test1', '', 1407914831, 1407914831, 0),
(181, 9, 4, 'dhddj', '', 1407914831, 1407914831, 0),
(182, 10, 2, 'dhfh', '', 1407914831, 1407914831, 0),
(183, 9, 6, 'Very Good', '', 1407914831, 1407914831, 0),
(184, 10, 3, 'dhdhxk', '', 1407914831, 1407914831, 0),
(185, 9, 7, 'Always', '', 1407914831, 1407914831, 0),
(186, 10, 4, 'dhffjj', '', 1407914831, 1407914831, 0),
(187, 9, 8, 'Always', '', 1407914831, 1407914831, 0),
(188, 10, 6, 'Very Good', '', 1407914831, 1407914831, 0),
(189, 9, 9, 'Strongly Agree', '', 1407914831, 1407914831, 0),
(190, 10, 7, 'Always', '', 1407914831, 1407914831, 0),
(191, 9, 10, 'Always', '', 1407914831, 1407914831, 0),
(192, 10, 8, 'Always', '', 1407914831, 1407914831, 0),
(193, 9, 11, 'Always', '', 1407914831, 1407914831, 0),
(194, 10, 9, 'Strongly Agree', '', 1407914831, 1407914831, 0),
(195, 9, 12, 'Always', '', 1407914831, 1407914831, 0),
(196, 10, 10, 'Always', '', 1407914831, 1407914831, 0),
(197, 9, 13, 'Strongly Agree', '', 1407914831, 1407914831, 0),
(198, 10, 11, 'Always', '', 1407914831, 1407914831, 0),
(199, 9, 14, 'Strongly Agree', '', 1407914831, 1407914831, 0),
(200, 10, 12, 'Always', '', 1407914831, 1407914831, 0),
(201, 9, 15, 'Always', '', 1407914831, 1407914831, 0),
(202, 10, 13, 'Strongly Agree', '', 1407914831, 1407914831, 0),
(203, 9, 16, 'Strongly Agree', '', 1407914831, 1407914831, 0),
(204, 10, 14, 'Strongly Agree', '', 1407914832, 1407914832, 0),
(205, 9, 17, 'Very important', '', 1407914832, 1407914832, 0),
(206, 10, 15, 'Always', '', 1407914832, 1407914832, 0),
(207, 9, 18, 'High Impact', '', 1407914832, 1407914832, 0),
(208, 10, 16, 'Strongly Agree', '', 1407914832, 1407914832, 0),
(209, 9, 19, 'Much better than expected', '', 1407914832, 1407914832, 0),
(210, 10, 17, 'Very important', '', 1407914832, 1407914832, 0),
(211, 9, 21, 'Very Good', '', 1407914832, 1407914832, 0),
(212, 10, 18, 'High Impact', '', 1407914832, 1407914832, 0),
(213, 9, 22, '', '', 1407914832, 1407914832, 0),
(214, 10, 19, 'Much better than expected', '', 1407914832, 1407914832, 0),
(215, 9, 23, '', '', 1407914832, 1407914832, 0),
(216, 10, 21, 'Very Good', '', 1407914832, 1407914832, 0),
(217, 9, 24, '', '', 1407914832, 1407914832, 0),
(218, 10, 22, '', '', 1407914832, 1407914832, 0),
(219, 10, 23, '', '', 1407914832, 1407914832, 0),
(220, 10, 24, '', '', 1407914832, 1407914832, 0),
(221, 11, 1, 'tes1', '', 1407914979, 1407914979, 0),
(222, 12, 1, 'test2', '', 1407914979, 1407914979, 0),
(223, 11, 2, 'dhdh', '', 1407914980, 1407914980, 0),
(224, 12, 2, 'dhdj', '', 1407914980, 1407914980, 0),
(225, 11, 3, 'dhdh', '', 1407914980, 1407914980, 0),
(226, 12, 3, 'djddj', '', 1407914980, 1407914980, 0),
(227, 11, 4, 'dhdh', '', 1407914980, 1407914980, 0),
(228, 12, 4, 'dhddj', '', 1407914980, 1407914980, 0),
(229, 11, 6, 'Very Good', '', 1407914980, 1407914980, 0),
(230, 12, 6, 'Very Good', '', 1407914980, 1407914980, 0),
(231, 11, 7, 'Always', '', 1407914980, 1407914980, 0),
(232, 12, 7, 'Always', '', 1407914980, 1407914980, 0),
(233, 11, 8, 'Always', '', 1407914980, 1407914980, 0),
(234, 12, 8, 'Always', '', 1407914980, 1407914980, 0),
(235, 11, 9, 'Strongly Agree', '', 1407914980, 1407914980, 0),
(236, 12, 9, 'Strongly Agree', '', 1407914980, 1407914980, 0),
(237, 11, 10, 'Always', '', 1407914980, 1407914980, 0),
(238, 12, 10, 'Always', '', 1407914980, 1407914980, 0),
(239, 11, 11, 'Always', '', 1407914980, 1407914980, 0),
(240, 12, 11, 'Always', '', 1407914980, 1407914980, 0),
(241, 11, 12, 'Always', '', 1407914980, 1407914980, 0),
(242, 12, 12, 'Always', '', 1407914980, 1407914980, 0),
(243, 11, 13, 'Strongly Agree', '', 1407914980, 1407914980, 0),
(244, 12, 13, 'Strongly Agree', '', 1407914980, 1407914980, 0),
(245, 11, 14, 'Strongly Agree', '', 1407914980, 1407914980, 0),
(246, 12, 14, 'Strongly Agree', '', 1407914980, 1407914980, 0),
(247, 11, 15, 'Always', '', 1407914980, 1407914980, 0),
(248, 12, 15, 'Always', '', 1407914980, 1407914980, 0),
(249, 11, 16, 'Strongly Agree', '', 1407914980, 1407914980, 0),
(250, 12, 16, 'Strongly Agree', '', 1407914980, 1407914980, 0),
(251, 11, 17, 'Very important', '', 1407914980, 1407914980, 0),
(252, 12, 17, 'Very important', '', 1407914980, 1407914980, 0),
(253, 11, 18, 'High Impact', '', 1407914980, 1407914980, 0),
(254, 12, 18, 'High Impact', '', 1407914981, 1407914981, 0),
(255, 11, 19, 'Much better than expected', '', 1407914981, 1407914981, 0),
(256, 12, 19, 'Much better than expected', '', 1407914981, 1407914981, 0),
(257, 11, 21, 'Very Good', '', 1407914981, 1407914981, 0),
(258, 12, 21, 'Very Good', '', 1407914981, 1407914981, 0),
(259, 11, 22, '', '', 1407914981, 1407914981, 0),
(260, 12, 22, '', '', 1407914981, 1407914981, 0),
(261, 11, 23, '', '', 1407914981, 1407914981, 0),
(262, 12, 23, '', '', 1407914981, 1407914981, 0),
(263, 11, 24, '', '', 1407914981, 1407914981, 0),
(264, 12, 24, '', '', 1407914981, 1407914981, 0),
(265, 13, 1, 'test2', '', 1407915507, 1407915507, 0),
(266, 14, 1, 'test1', '', 1407915507, 1407915507, 0),
(267, 13, 2, 'djdj', '', 1407915507, 1407915507, 0),
(268, 14, 2, 'dhdjdj', '', 1407915507, 1407915507, 0),
(269, 13, 3, 'dhdj', '', 1407915507, 1407915507, 0),
(270, 14, 3, 'dhdjdi', '', 1407915507, 1407915507, 0),
(271, 13, 4, 'dhdj', '', 1407915507, 1407915507, 0),
(272, 14, 4, 'dhdhdu', '', 1407915507, 1407915507, 0),
(273, 13, 6, 'Very Good', '', 1407915507, 1407915507, 0),
(274, 14, 6, 'Very Good', '', 1407915507, 1407915507, 0),
(275, 13, 7, 'Always', '', 1407915507, 1407915507, 0),
(276, 14, 7, 'Always', '', 1407915507, 1407915507, 0),
(277, 13, 8, 'Always', '', 1407915507, 1407915507, 0),
(278, 14, 8, 'Always', '', 1407915507, 1407915507, 0),
(279, 13, 9, 'Strongly Agree', '', 1407915508, 1407915508, 0),
(280, 14, 9, 'Strongly Agree', '', 1407915508, 1407915508, 0),
(281, 13, 10, 'Always', '', 1407915508, 1407915508, 0),
(282, 14, 10, 'Always', '', 1407915508, 1407915508, 0),
(283, 13, 11, 'Always', '', 1407915508, 1407915508, 0),
(284, 14, 11, 'Always', '', 1407915508, 1407915508, 0),
(285, 13, 12, 'Always', '', 1407915508, 1407915508, 0),
(286, 14, 12, 'Always', '', 1407915508, 1407915508, 0),
(287, 13, 13, 'Strongly Agree', '', 1407915508, 1407915508, 0),
(288, 14, 13, 'Strongly Agree', '', 1407915508, 1407915508, 0),
(289, 13, 14, 'Strongly Agree', '', 1407915508, 1407915508, 0),
(290, 14, 14, 'Strongly Agree', '', 1407915508, 1407915508, 0),
(291, 13, 15, 'Always', '', 1407915508, 1407915508, 0),
(292, 14, 15, 'Always', '', 1407915508, 1407915508, 0),
(293, 13, 16, 'Strongly Agree', '', 1407915508, 1407915508, 0),
(294, 14, 16, 'Strongly Agree', '', 1407915508, 1407915508, 0),
(295, 13, 17, 'Very important', '', 1407915508, 1407915508, 0),
(296, 14, 17, 'Very important', '', 1407915508, 1407915508, 0),
(297, 13, 18, 'High Impact', '', 1407915508, 1407915508, 0),
(298, 14, 18, 'High Impact', '', 1407915508, 1407915508, 0),
(299, 13, 19, 'Much better than expected', '', 1407915508, 1407915508, 0),
(300, 14, 19, 'Much better than expected', '', 1407915508, 1407915508, 0),
(301, 13, 21, 'Very Good', '', 1407915508, 1407915508, 0),
(302, 14, 21, 'Very Good', '', 1407915508, 1407915508, 0),
(303, 13, 22, '', '', 1407915508, 1407915508, 0),
(304, 14, 22, '', '', 1407915509, 1407915509, 0),
(305, 13, 23, '', '', 1407915509, 1407915509, 0),
(306, 14, 23, '', '', 1407915509, 1407915509, 0),
(307, 13, 24, '', '', 1407915509, 1407915509, 0),
(308, 14, 24, '', '', 1407915509, 1407915509, 0),
(309, 15, 1, 'janet', '', 1408025356, 1408025356, 0),
(310, 15, 2, 'sacred heart', '', 1408025356, 1408025356, 0),
(311, 15, 3, 'mandaue', '', 1408025356, 1408025356, 0),
(312, 15, 4, 'teacher', '', 1408025356, 1408025356, 0),
(313, 15, 6, 'Very Good', '', 1408025357, 1408025357, 0),
(314, 15, 7, 'Always', '', 1408025357, 1408025357, 0),
(315, 15, 8, 'Always', '', 1408025357, 1408025357, 0),
(316, 15, 9, 'Strongly Agree', '', 1408025357, 1408025357, 0),
(317, 15, 10, 'Always', '', 1408025357, 1408025357, 0),
(318, 15, 11, 'Always', '', 1408025357, 1408025357, 0),
(319, 15, 12, 'Always', '', 1408025357, 1408025357, 0),
(320, 15, 13, 'Strongly Agree', '', 1408025357, 1408025357, 0),
(321, 15, 14, 'Strongly Agree', '', 1408025357, 1408025357, 0),
(322, 15, 15, 'Always', '', 1408025357, 1408025357, 0),
(323, 15, 16, 'Strongly Agree', '', 1408025357, 1408025357, 0),
(324, 15, 17, 'Very important', '', 1408025357, 1408025357, 0),
(325, 15, 18, 'High Impact', '', 1408025357, 1408025357, 0),
(326, 15, 19, 'Much better than expected', '', 1408025357, 1408025357, 0),
(327, 15, 21, 'Very Good', '', 1408025357, 1408025357, 0),
(328, 15, 22, '', '20140814_220805.mp4', 1408025357, 1408025357, 0),
(329, 15, 23, '', '20140814_220815.mp4', 1408025357, 1408025357, 0),
(330, 15, 24, '', '20140814_220822.mp4', 1408025357, 1408025357, 0);

-- --------------------------------------------------------

--
-- Table structure for table `question_items`
--

DROP TABLE IF EXISTS `question_items`;
CREATE TABLE IF NOT EXISTS `question_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `question_items`
--

INSERT INTO `question_items` (`id`, `parent_id`, `question_id`, `title`, `created`, `modified`) VALUES
(1, 0, 5, 'Training of Providers', '2014-07-08 16:41:13', '2014-07-08 16:41:13'),
(2, 1, 5, 'Post-Partum IUD insertion', '2014-07-08 16:41:42', '2014-07-08 16:41:42'),
(3, 1, 5, 'Sub-dermal Implant insertion', '2014-07-09 02:12:45', '2014-07-09 02:12:45'),
(4, 1, 5, 'Usapan Facilitators', '2014-07-09 02:12:59', '2014-07-09 02:12:59'),
(5, 1, 5, 'Gender Awareness and  Transformative Programming', '2014-07-09 02:13:09', '2014-07-09 02:13:09'),
(6, 0, 5, 'Training of Trainers', '2014-07-09 02:13:31', '2014-07-09 02:13:31'),
(7, 6, 5, 'Post-Partum IUD insertion', '2014-07-09 02:13:57', '2014-07-09 02:32:59'),
(8, 6, 5, 'Sub-dermal Implant insertion', '2014-07-09 02:14:03', '2014-07-09 02:32:58'),
(9, 6, 5, 'Gender Awareness and  Transformative Programming', '2014-07-09 02:14:10', '2014-07-09 02:32:56'),
(10, 0, 5, 'Usapan', '2014-07-09 02:14:59', '2014-07-09 02:14:59'),
(11, 0, 5, 'Buntis celebration', '2014-07-09 02:15:06', '2014-07-09 02:15:06'),
(12, 0, 5, 'Pregnancy Tracking', '2014-07-09 02:15:15', '2014-07-09 02:15:15'),
(13, 0, 5, 'Ambulatory BTL services', '2014-07-09 02:15:22', '2014-07-09 02:15:22'),
(14, 0, 5, 'Others', '2014-07-09 02:15:30', '2014-07-09 02:15:30'),
(15, 14, 5, 'Clinical Case conference', '2014-07-09 02:16:03', '2014-07-09 04:37:43'),
(16, 0, 20, 'Activities in the previous year have  been sufficiently coordinated with your   organization', '2014-07-09 05:33:41', '2014-07-09 05:33:41'),
(17, 0, 20, 'VisayasHealth management, technical,  and administrative  contacted', '2014-07-09 05:33:51', '2014-07-09 05:33:51');

-- --------------------------------------------------------

--
-- Table structure for table `question_item_answers`
--

DROP TABLE IF EXISTS `question_item_answers`;
CREATE TABLE IF NOT EXISTS `question_item_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submission_id` int(11) NOT NULL,
  `question_item_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `created` int(11) NOT NULL,
  `modified` int(11) NOT NULL,
  `stat` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `question_item_answers`
--

INSERT INTO `question_item_answers` (`id`, `submission_id`, `question_item_id`, `option_id`, `value`, `created`, `modified`, `stat`) VALUES
(1, 15, 2, 26, 'Relevant VisayasHealth products/services you are AWARE of', 1408025356, 1408025356, 0),
(2, 15, 3, 26, 'Relevant VisayasHealth products/services you are AWARE of', 1408025357, 1408025357, 0),
(3, 15, 4, 26, 'Relevant VisayasHealth products/services you are AWARE of', 1408025357, 1408025357, 0),
(4, 15, 5, 27, 'VisayasHealth products and services USED in the previous year', 1408025357, 1408025357, 0),
(5, 15, 7, 27, 'VisayasHealth products and services USED in the previous year', 1408025357, 1408025357, 0),
(6, 15, 8, 26, 'Relevant VisayasHealth products/services you are AWARE of', 1408025357, 1408025357, 0),
(7, 15, 9, 26, 'Relevant VisayasHealth products/services you are AWARE of', 1408025357, 1408025357, 0),
(8, 15, 10, 27, 'VisayasHealth products and services USED in the previous year', 1408025357, 1408025357, 0),
(9, 15, 11, 27, 'VisayasHealth products and services USED in the previous year', 1408025357, 1408025357, 0),
(10, 15, 12, 27, 'VisayasHealth products and services USED in the previous year', 1408025357, 1408025357, 0),
(11, 15, 13, 27, 'VisayasHealth products and services USED in the previous year', 1408025357, 1408025357, 0),
(12, 15, 15, 27, 'VisayasHealth products and services USED in the previous year', 1408025357, 1408025357, 0),
(13, 15, 16, 21, 'Strongly Agree', 1408025357, 1408025357, 0),
(14, 15, 17, 21, 'Strongly Agree', 1408025357, 1408025357, 0);

-- --------------------------------------------------------

--
-- Table structure for table `question_options`
--

DROP TABLE IF EXISTS `question_options`;
CREATE TABLE IF NOT EXISTS `question_options` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `question_id` int(10) NOT NULL,
  `sequence_id` int(10) DEFAULT NULL,
  `options` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `weight` int(10) DEFAULT NULL,
  `point` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `submissions`
--

DROP TABLE IF EXISTS `submissions`;
CREATE TABLE IF NOT EXISTS `submissions` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) NOT NULL,
  `user_id` int(10) NOT NULL,
  `enumerator` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `raw_data` text COLLATE utf8_unicode_ci NOT NULL,
  `point` int(10) DEFAULT NULL,
  `valid` tinyint(1) DEFAULT '1',
  `latitude` decimal(10,8) NOT NULL,
  `longitude` decimal(11,8) NOT NULL,
  `map_api_url` text COLLATE utf8_unicode_ci NOT NULL,
  `image_encoding` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Dumping data for table `submissions`
--

INSERT INTO `submissions` (`id`, `survey_id`, `user_id`, `enumerator`, `raw_data`, `point`, `valid`, `latitude`, `longitude`, `map_api_url`, `image_encoding`, `created`, `modified`, `status`) VALUES
(1, 1, 1, 'mike', '{"survey_id":"1","enumerator":"mike","latitude":"10.3178463","longitude":"123.907018","map_api_url":"http:\\/\\/maps.googleapis.com\\/maps\\/api\\/geocode\\/json?latlng=10.3178463,123.907018&sensor=false","answer":{"1":"test2","2":"hdje","3":"heue","4":"ueh","5":{"2":{"26":"No","27":"No"},"3":{"26":"No","27":"No"},"4":{"26":"No","27":"No"},"5":{"26":"No","27":"No"},"7":{"26":"No","27":"No"},"8":{"26":"No","27":"No"},"9":{"26":"No","27":"No"},"10":{"26":"No","27":"No"},"11":{"26":"No","27":"No"},"12":{"26":"No","27":"No"},"13":{"26":"No","27":"No"},"15":{"26":"No","27":"No"}},"6":"Very Good","7":"Always","8":"Always","9":"Strongly Agree","10":"Always","11":"Always","12":"Always","13":"Strongly Agree","14":"Strongly Agree","15":"Always","16":"Strongly Agree","17":"Very important","18":"High Impact","19":"Much better than expected","20":{"16":{"21":"No","22":"No","23":"No","24":"No","25":"No"},"17":{"21":"No","22":"No","23":"No","24":"No","25":"No"}},"21":"Very Good","22":"","23":"","24":""}}', NULL, 1, '10.31784630', '123.90701800', 'http://maps.googleapis.com/maps/api/geocode/json?latlng=10.3178463,123.907018&sensor=false', 'test2_20140813_131207.jpg', '2014-08-13 05:13:32', '2014-08-15 02:02:40', 'active'),
(2, 1, 1, 'mike', '{"survey_id":"1","enumerator":"mike","latitude":"10.3178463","longitude":"123.907018","map_api_url":"http:\\/\\/maps.googleapis.com\\/maps\\/api\\/geocode\\/json?latlng=10.3178463,123.907018&sensor=false","answer":{"1":"test1","2":"hdhbr","3":"hfhfb","4":"hfhfh","5":{"2":{"26":"No","27":"No"},"3":{"26":"No","27":"No"},"4":{"26":"No","27":"No"},"5":{"26":"No","27":"No"},"7":{"26":"No","27":"No"},"8":{"26":"No","27":"No"},"9":{"26":"No","27":"No"},"10":{"26":"No","27":"No"},"11":{"26":"No","27":"No"},"12":{"26":"No","27":"No"},"13":{"26":"No","27":"No"},"15":{"26":"No","27":"No"}},"6":"Very Good","7":"Always","8":"Always","9":"Strongly Agree","10":"Always","11":"Always","12":"Always","13":"Strongly Agree","14":"Strongly Agree","15":"Always","16":"Strongly Agree","17":"Very important","18":"High Impact","19":"Much better than expected","20":{"16":{"21":"No","22":"No","23":"No","24":"No","25":"No"},"17":{"21":"No","22":"No","23":"No","24":"No","25":"No"}},"21":"Very Good","22":"","23":"","24":""}}', NULL, 1, '10.31784630', '123.90701800', 'http://maps.googleapis.com/maps/api/geocode/json?latlng=10.3178463,123.907018&sensor=false', 'test1_20140813_131120.jpg', '2014-08-13 05:13:34', '2014-08-15 02:02:40', 'active'),
(3, 1, 1, 'mike', '{"survey_id":"1","enumerator":"mike","latitude":"10.3178385","longitude":"123.9070142","map_api_url":"http:\\/\\/maps.googleapis.com\\/maps\\/api\\/geocode\\/json?latlng=10.3178385,123.9070142&sensor=false","answer":{"1":"test1","2":"hfhd","3":"hdhdj","4":"dhdjdj","5":{"2":{"26":"No","27":"No"},"3":{"26":"No","27":"No"},"4":{"26":"No","27":"No"},"5":{"26":"No","27":"No"},"7":{"26":"No","27":"No"},"8":{"26":"No","27":"No"},"9":{"26":"No","27":"No"},"10":{"26":"No","27":"No"},"11":{"26":"No","27":"No"},"12":{"26":"No","27":"No"},"13":{"26":"No","27":"No"},"15":{"26":"No","27":"No"}},"6":"Very Good","7":"Always","8":"Always","9":"Strongly Agree","10":"Always","11":"Always","12":"Always","13":"Strongly Agree","14":"Strongly Agree","15":"Always","16":"Strongly Agree","17":"Very important","18":"High Impact","19":"Much better than expected","20":{"16":{"21":"No","22":"No","23":"No","24":"No","25":"No"},"17":{"21":"No","22":"No","23":"No","24":"No","25":"No"}},"21":"Very Good","22":"","23":"","24":""}}', NULL, 1, '10.31783850', '123.90701420', 'http://maps.googleapis.com/maps/api/geocode/json?latlng=10.3178385,123.9070142&sensor=false', 'test1_20140813_150531.jpg', '2014-08-13 07:06:56', '2014-08-15 02:02:40', 'active'),
(4, 1, 1, 'mike', '{"survey_id":"1","enumerator":"mike","latitude":"10.3178385","longitude":"123.9070142","map_api_url":"http:\\/\\/maps.googleapis.com\\/maps\\/api\\/geocode\\/json?latlng=10.3178385,123.9070142&sensor=false","answer":{"1":"test2","2":"hsnd","3":"dddd","4":"dddd","5":{"2":{"26":"No","27":"No"},"3":{"26":"No","27":"No"},"4":{"26":"No","27":"No"},"5":{"26":"No","27":"No"},"7":{"26":"No","27":"No"},"8":{"26":"No","27":"No"},"9":{"26":"No","27":"No"},"10":{"26":"No","27":"No"},"11":{"26":"No","27":"No"},"12":{"26":"No","27":"No"},"13":{"26":"No","27":"No"},"15":{"26":"No","27":"No"}},"6":"Very Good","7":"Always","8":"Always","9":"Strongly Agree","10":"Always","11":"Always","12":"Always","13":"Strongly Agree","14":"Strongly Agree","15":"Always","16":"Strongly Agree","17":"Very important","18":"High Impact","19":"Much better than expected","20":{"16":{"21":"No","22":"No","23":"No","24":"No","25":"No"},"17":{"21":"No","22":"No","23":"No","24":"No","25":"No"}},"21":"Very Good","22":"","23":"","24":""}}', NULL, 1, '10.31783850', '123.90701420', 'http://maps.googleapis.com/maps/api/geocode/json?latlng=10.3178385,123.9070142&sensor=false', 'test2_20140813_150551.jpg', '2014-08-13 07:06:57', '2014-08-15 02:02:40', 'active'),
(5, 1, 1, 'mike', '{"survey_id":"1","enumerator":"mike","latitude":"10.3178636","longitude":"123.9070107","map_api_url":"http:\\/\\/maps.googleapis.com\\/maps\\/api\\/geocode\\/json?latlng=10.3178636,123.9070107&sensor=false","answer":{"1":"test2","2":"hfh","3":"hbu","4":"hjf","5":{"2":{"26":"No","27":"No"},"3":{"26":"No","27":"No"},"4":{"26":"No","27":"No"},"5":{"26":"No","27":"No"},"7":{"26":"No","27":"No"},"8":{"26":"No","27":"No"},"9":{"26":"No","27":"No"},"10":{"26":"No","27":"No"},"11":{"26":"No","27":"No"},"12":{"26":"No","27":"No"},"13":{"26":"No","27":"No"},"15":{"26":"No","27":"No"}},"6":"Very Good","7":"Always","8":"Always","9":"Strongly Agree","10":"Always","11":"Always","12":"Always","13":"Strongly Agree","14":"Strongly Agree","15":"Always","16":"Strongly Agree","17":"Very important","18":"High Impact","19":"Much better than expected","20":{"16":{"21":"No","22":"No","23":"No","24":"No","25":"No"},"17":{"21":"No","22":"No","23":"No","24":"No","25":"No"}},"21":"Very Good","22":"","23":"","24":""}}', NULL, 1, '10.31786360', '123.90701070', 'http://maps.googleapis.com/maps/api/geocode/json?latlng=10.3178636,123.9070107&sensor=false', 'test2_20140813_144551.jpg', '2014-08-13 07:06:58', '2014-08-15 02:02:40', 'active'),
(6, 1, 1, 'mike', '{"survey_id":"1","enumerator":"mike","latitude":"10.3178636","longitude":"123.9070107","map_api_url":"http:\\/\\/maps.googleapis.com\\/maps\\/api\\/geocode\\/json?latlng=10.3178636,123.9070107&sensor=false","answer":{"1":"test1","2":"qwerty","3":"qwerty","4":"qaws","5":{"2":{"26":"No","27":"No"},"3":{"26":"No","27":"No"},"4":{"26":"No","27":"No"},"5":{"26":"No","27":"No"},"7":{"26":"No","27":"No"},"8":{"26":"No","27":"No"},"9":{"26":"No","27":"No"},"10":{"26":"No","27":"No"},"11":{"26":"No","27":"No"},"12":{"26":"No","27":"No"},"13":{"26":"No","27":"No"},"15":{"26":"No","27":"No"}},"6":"Very Good","7":"Always","8":"Always","9":"Strongly Agree","10":"Always","11":"Always","12":"Always","13":"Strongly Agree","14":"Strongly Agree","15":"Always","16":"Strongly Agree","17":"Very important","18":"High Impact","19":"Much better than expected","20":{"16":{"21":"No","22":"No","23":"No","24":"No","25":"No"},"17":{"21":"No","22":"No","23":"No","24":"No","25":"No"}},"21":"Very Good","22":"","23":"","24":""}}', NULL, 1, '10.31786360', '123.90701070', 'http://maps.googleapis.com/maps/api/geocode/json?latlng=10.3178636,123.9070107&sensor=false', 'test1_20140813_144521.jpg', '2014-08-13 07:06:59', '2014-08-15 02:02:40', 'active'),
(7, 1, 1, 'mike', '{"survey_id":"1","enumerator":"mike","latitude":"10.317837","longitude":"123.9070344","map_api_url":"http:\\/\\/maps.googleapis.com\\/maps\\/api\\/geocode\\/json?latlng=10.317837,123.9070344&sensor=false","answer":{"1":"test1","2":"gdhd","3":"dhdhd","4":"dhdhd","5":{"2":{"26":"No","27":"No"},"3":{"26":"No","27":"No"},"4":{"26":"No","27":"No"},"5":{"26":"No","27":"No"},"7":{"26":"No","27":"No"},"8":{"26":"No","27":"No"},"9":{"26":"No","27":"No"},"10":{"26":"No","27":"No"},"11":{"26":"No","27":"No"},"12":{"26":"No","27":"No"},"13":{"26":"No","27":"No"},"15":{"26":"No","27":"No"}},"6":"Very Good","7":"Always","8":"Always","9":"Strongly Agree","10":"Always","11":"Always","12":"Always","13":"Strongly Agree","14":"Strongly Agree","15":"Always","16":"Strongly Agree","17":"Very important","18":"High Impact","19":"Much better than expected","20":{"16":{"21":"No","22":"No","23":"No","24":"No","25":"No"},"17":{"21":"No","22":"No","23":"No","24":"No","25":"No"}},"21":"Very Good","22":"","23":"","24":""}}', NULL, 1, '10.31783700', '123.90703440', 'http://maps.googleapis.com/maps/api/geocode/json?latlng=10.317837,123.9070344&sensor=false', 'test1_20140813_151732.jpg', '2014-08-13 07:19:28', '2014-08-15 02:02:40', 'active'),
(8, 1, 1, 'mike', '{"survey_id":"1","enumerator":"mike","latitude":"10.317837","longitude":"123.9070344","map_api_url":"http:\\/\\/maps.googleapis.com\\/maps\\/api\\/geocode\\/json?latlng=10.317837,123.9070344&sensor=false","answer":{"1":"test2","2":"bdhdj","3":"dhdh","4":"dhdjd","5":{"2":{"26":"No","27":"No"},"3":{"26":"No","27":"No"},"4":{"26":"No","27":"No"},"5":{"26":"No","27":"No"},"7":{"26":"No","27":"No"},"8":{"26":"No","27":"No"},"9":{"26":"No","27":"No"},"10":{"26":"No","27":"No"},"11":{"26":"No","27":"No"},"12":{"26":"No","27":"No"},"13":{"26":"No","27":"No"},"15":{"26":"No","27":"No"}},"6":"Very Good","7":"Always","8":"Always","9":"Strongly Agree","10":"Always","11":"Always","12":"Always","13":"Strongly Agree","14":"Strongly Agree","15":"Always","16":"Strongly Agree","17":"Very important","18":"High Impact","19":"Much better than expected","20":{"16":{"21":"No","22":"No","23":"No","24":"No","25":"No"},"17":{"21":"No","22":"No","23":"No","24":"No","25":"No"}},"21":"Very Good","22":"","23":"","24":""}}', NULL, 1, '10.31783700', '123.90703440', 'http://maps.googleapis.com/maps/api/geocode/json?latlng=10.317837,123.9070344&sensor=false', 'test2_20140813_151817.jpg', '2014-08-13 07:19:28', '2014-08-15 02:02:40', 'active'),
(9, 1, 1, 'mike', '{"survey_id":"1","enumerator":"mike","latitude":"10.317864","longitude":"123.9070226","map_api_url":"http:\\/\\/maps.googleapis.com\\/maps\\/api\\/geocode\\/json?latlng=10.317864,123.9070226&sensor=false","answer":{"1":"tst2","2":"rhdjd","3":"dhejr","4":"dhddj","5":{"2":{"26":"No","27":"No"},"3":{"26":"No","27":"No"},"4":{"26":"No","27":"No"},"5":{"26":"No","27":"No"},"7":{"26":"No","27":"No"},"8":{"26":"No","27":"No"},"9":{"26":"No","27":"No"},"10":{"26":"No","27":"No"},"11":{"26":"No","27":"No"},"12":{"26":"No","27":"No"},"13":{"26":"No","27":"No"},"15":{"26":"No","27":"No"}},"6":"Very Good","7":"Always","8":"Always","9":"Strongly Agree","10":"Always","11":"Always","12":"Always","13":"Strongly Agree","14":"Strongly Agree","15":"Always","16":"Strongly Agree","17":"Very important","18":"High Impact","19":"Much better than expected","20":{"16":{"21":"No","22":"No","23":"No","24":"No","25":"No"},"17":{"21":"No","22":"No","23":"No","24":"No","25":"No"}},"21":"Very Good","22":"","23":"","24":""}}', NULL, 1, '10.31786400', '123.90702260', 'http://maps.googleapis.com/maps/api/geocode/json?latlng=10.317864,123.9070226&sensor=false', 'tst2_20140813_152609.jpg', '2014-08-13 07:27:11', '2014-08-15 02:02:40', 'active'),
(10, 1, 1, 'mike', '{"survey_id":"1","enumerator":"mike","latitude":"10.317864","longitude":"123.9070226","map_api_url":"http:\\/\\/maps.googleapis.com\\/maps\\/api\\/geocode\\/json?latlng=10.317864,123.9070226&sensor=false","answer":{"1":"test1","2":"dhfh","3":"dhdhxk","4":"dhffjj","5":{"2":{"26":"No","27":"No"},"3":{"26":"No","27":"No"},"4":{"26":"No","27":"No"},"5":{"26":"No","27":"No"},"7":{"26":"No","27":"No"},"8":{"26":"No","27":"No"},"9":{"26":"No","27":"No"},"10":{"26":"No","27":"No"},"11":{"26":"No","27":"No"},"12":{"26":"No","27":"No"},"13":{"26":"No","27":"No"},"15":{"26":"No","27":"No"}},"6":"Very Good","7":"Always","8":"Always","9":"Strongly Agree","10":"Always","11":"Always","12":"Always","13":"Strongly Agree","14":"Strongly Agree","15":"Always","16":"Strongly Agree","17":"Very important","18":"High Impact","19":"Much better than expected","20":{"16":{"21":"No","22":"No","23":"No","24":"No","25":"No"},"17":{"21":"No","22":"No","23":"No","24":"No","25":"No"}},"21":"Very Good","22":"","23":"","24":""}}', NULL, 1, '10.31786400', '123.90702260', 'http://maps.googleapis.com/maps/api/geocode/json?latlng=10.317864,123.9070226&sensor=false', 'test1_20140813_152536.jpg', '2014-08-13 07:27:11', '2014-08-15 02:02:40', 'active'),
(11, 1, 1, 'mike', '{"survey_id":"1","enumerator":"mike","latitude":"10.3178437","longitude":"123.9070144","map_api_url":"http:\\/\\/maps.googleapis.com\\/maps\\/api\\/geocode\\/json?latlng=10.3178437,123.9070144&sensor=false","answer":{"1":"tes1","2":"dhdh","3":"dhdh","4":"dhdh","5":{"2":{"26":"No","27":"No"},"3":{"26":"No","27":"No"},"4":{"26":"No","27":"No"},"5":{"26":"No","27":"No"},"7":{"26":"No","27":"No"},"8":{"26":"No","27":"No"},"9":{"26":"No","27":"No"},"10":{"26":"No","27":"No"},"11":{"26":"No","27":"No"},"12":{"26":"No","27":"No"},"13":{"26":"No","27":"No"},"15":{"26":"No","27":"No"}},"6":"Very Good","7":"Always","8":"Always","9":"Strongly Agree","10":"Always","11":"Always","12":"Always","13":"Strongly Agree","14":"Strongly Agree","15":"Always","16":"Strongly Agree","17":"Very important","18":"High Impact","19":"Much better than expected","20":{"16":{"21":"No","22":"No","23":"No","24":"No","25":"No"},"17":{"21":"No","22":"No","23":"No","24":"No","25":"No"}},"21":"Very Good","22":"","23":"","24":""}}', NULL, 1, '10.31784370', '123.90701440', 'http://maps.googleapis.com/maps/api/geocode/json?latlng=10.3178437,123.9070144&sensor=false', 'tes1_20140813_152814.jpg', '2014-08-13 07:29:39', '2014-08-15 02:02:40', 'active'),
(12, 1, 1, 'mike', '{"survey_id":"1","enumerator":"mike","latitude":"10.3178437","longitude":"123.9070144","map_api_url":"http:\\/\\/maps.googleapis.com\\/maps\\/api\\/geocode\\/json?latlng=10.3178437,123.9070144&sensor=false","answer":{"1":"test2","2":"dhdj","3":"djddj","4":"dhddj","5":{"2":{"26":"No","27":"No"},"3":{"26":"No","27":"No"},"4":{"26":"No","27":"No"},"5":{"26":"No","27":"No"},"7":{"26":"No","27":"No"},"8":{"26":"No","27":"No"},"9":{"26":"No","27":"No"},"10":{"26":"No","27":"No"},"11":{"26":"No","27":"No"},"12":{"26":"No","27":"No"},"13":{"26":"No","27":"No"},"15":{"26":"No","27":"No"}},"6":"Very Good","7":"Always","8":"Always","9":"Strongly Agree","10":"Always","11":"Always","12":"Always","13":"Strongly Agree","14":"Strongly Agree","15":"Always","16":"Strongly Agree","17":"Very important","18":"High Impact","19":"Much better than expected","20":{"16":{"21":"No","22":"No","23":"No","24":"No","25":"No"},"17":{"21":"No","22":"No","23":"No","24":"No","25":"No"}},"21":"Very Good","22":"","23":"","24":""}}', NULL, 1, '10.31784370', '123.90701440', 'http://maps.googleapis.com/maps/api/geocode/json?latlng=10.3178437,123.9070144&sensor=false', 'test2_20140813_152835.jpg', '2014-08-13 07:29:39', '2014-08-15 02:02:40', 'active'),
(13, 1, 1, 'mike', '{"survey_id":"1","enumerator":"mike","latitude":"10.31785","longitude":"123.9070346","map_api_url":"http:\\/\\/maps.googleapis.com\\/maps\\/api\\/geocode\\/json?latlng=10.31785,123.9070346&sensor=false","answer":{"1":"test2","2":"djdj","3":"dhdj","4":"dhdj","5":{"2":{"26":"No","27":"No"},"3":{"26":"No","27":"No"},"4":{"26":"No","27":"No"},"5":{"26":"No","27":"No"},"7":{"26":"No","27":"No"},"8":{"26":"No","27":"No"},"9":{"26":"No","27":"No"},"10":{"26":"No","27":"No"},"11":{"26":"No","27":"No"},"12":{"26":"No","27":"No"},"13":{"26":"No","27":"No"},"15":{"26":"No","27":"No"}},"6":"Very Good","7":"Always","8":"Always","9":"Strongly Agree","10":"Always","11":"Always","12":"Always","13":"Strongly Agree","14":"Strongly Agree","15":"Always","16":"Strongly Agree","17":"Very important","18":"High Impact","19":"Much better than expected","20":{"16":{"21":"No","22":"No","23":"No","24":"No","25":"No"},"17":{"21":"No","22":"No","23":"No","24":"No","25":"No"}},"21":"Very Good","22":"","23":"","24":""}}', NULL, 1, '10.31785000', '123.90703460', 'http://maps.googleapis.com/maps/api/geocode/json?latlng=10.31785,123.9070346&sensor=false', 'test2_20140813_153612.jpg', '2014-08-13 07:38:27', '2014-08-15 02:02:40', 'active'),
(14, 1, 1, 'mike', '{"survey_id":"1","enumerator":"mike","latitude":"10.31785","longitude":"123.9070346","map_api_url":"http:\\/\\/maps.googleapis.com\\/maps\\/api\\/geocode\\/json?latlng=10.31785,123.9070346&sensor=false","answer":{"1":"test1","2":"dhdjdj","3":"dhdjdi","4":"dhdhdu","5":{"2":{"26":"No","27":"No"},"3":{"26":"No","27":"No"},"4":{"26":"No","27":"No"},"5":{"26":"No","27":"No"},"7":{"26":"No","27":"No"},"8":{"26":"No","27":"No"},"9":{"26":"No","27":"No"},"10":{"26":"No","27":"No"},"11":{"26":"No","27":"No"},"12":{"26":"No","27":"No"},"13":{"26":"No","27":"No"},"15":{"26":"No","27":"No"}},"6":"Very Good","7":"Always","8":"Always","9":"Strongly Agree","10":"Always","11":"Always","12":"Always","13":"Strongly Agree","14":"Strongly Agree","15":"Always","16":"Strongly Agree","17":"Very important","18":"High Impact","19":"Much better than expected","20":{"16":{"21":"No","22":"No","23":"No","24":"No","25":"No"},"17":{"21":"No","22":"No","23":"No","24":"No","25":"No"}},"21":"Very Good","22":"","23":"","24":""}}', NULL, 1, '10.31785000', '123.90703460', 'http://maps.googleapis.com/maps/api/geocode/json?latlng=10.31785,123.9070346&sensor=false', 'test1_20140813_153554.jpg', '2014-08-13 07:38:27', '2014-08-15 02:02:40', 'active'),
(15, 1, 1, 'albert', '{"survey_id":"1","enumerator":"albert","latitude":"10.260503232999897","longitude":"123.84713825427771","map_api_url":"http:\\/\\/maps.googleapis.com\\/maps\\/api\\/geocode\\/json?latlng=10.260503232999897,123.84713825427771&sensor=false","answer":{"1":"janet","2":"sacred heart","3":"mandaue","4":"teacher","5":{"2":{"26":"Yes","27":"No"},"3":{"26":"Yes","27":"No"},"4":{"26":"Yes","27":"No"},"5":{"26":"No","27":"Yes"},"7":{"26":"No","27":"Yes"},"8":{"26":"Yes","27":"No"},"9":{"26":"Yes","27":"No"},"10":{"26":"No","27":"Yes"},"11":{"26":"No","27":"Yes"},"12":{"26":"No","27":"Yes"},"13":{"26":"No","27":"Yes"},"15":{"26":"No","27":"Yes"}},"6":"Very Good","7":"Always","8":"Always","9":"Strongly Agree","10":"Always","11":"Always","12":"Always","13":"Strongly Agree","14":"Strongly Agree","15":"Always","16":"Strongly Agree","17":"Very important","18":"High Impact","19":"Much better than expected","20":{"16":{"21":"Yes","22":"No","23":"No","24":"No","25":"No"},"17":{"21":"Yes","22":"No","23":"No","24":"No","25":"No"}},"21":"Very Good","22":"","23":"","24":""}}', NULL, 1, '10.26050323', '123.84713825', 'http://maps.googleapis.com/maps/api/geocode/json?latlng=10.260503232999897,123.84713825427771&sensor=false', 'janet_20140814_220830.jpg', '2014-08-14 14:09:16', '2014-08-15 02:02:40', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `submission_details`
--

DROP TABLE IF EXISTS `submission_details`;
CREATE TABLE IF NOT EXISTS `submission_details` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `question_id` int(10) NOT NULL,
  `submission_id` int(10) NOT NULL,
  `sequence_id` int(10) NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `point` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `surveys`
--

DROP TABLE IF EXISTS `surveys`;
CREATE TABLE IF NOT EXISTS `surveys` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `surveys`
--

INSERT INTO `surveys` (`id`, `title`, `created`, `modified`) VALUES
(1, 'CUSTOMER SERVICE ASSESSMENT - PARTNER LEVEL (DOH-RO: RD, ARD, MNCHN COORDINATOR)', '2014-07-08 15:24:10', '2014-07-08 15:24:10');

-- --------------------------------------------------------

--
-- Table structure for table `survey_forms`
--

DROP TABLE IF EXISTS `survey_forms`;
CREATE TABLE IF NOT EXISTS `survey_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT '0',
  `survey_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `is_profile` tinyint(1) NOT NULL DEFAULT '0',
  `sequence_no` int(11) NOT NULL DEFAULT '0',
  `question_count` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `survey_forms`
--

INSERT INTO `survey_forms` (`id`, `parent_id`, `survey_id`, `user_id`, `title`, `description`, `is_profile`, `sequence_no`, `question_count`, `created`, `modified`, `status`) VALUES
(1, 0, 1, 0, 'Organization Profile', 'Organization Profile', 0, 1, 4, '2014-07-08 15:25:01', '2014-07-08 15:37:30', 'active'),
(2, 0, 1, 0, 'Service Dimensions', 'Service Dimensions', 0, 2, 0, '2014-07-08 15:25:17', '2014-07-08 15:25:17', 'active'),
(3, 2, 1, 0, 'Utilization of Products and Services', 'Utilization of Products and Services', 0, 1, 1, '2014-07-08 15:31:18', '2014-07-08 15:31:18', 'active'),
(4, 2, 1, 0, 'Technical assistance', 'Technical assistance', 0, 2, 4, '2014-07-08 15:34:56', '2014-07-08 15:44:26', 'active'),
(5, 2, 1, 0, 'Service Orientation', 'Service Orientation', 0, 3, 6, '2014-07-08 15:38:48', '2014-07-08 15:44:46', 'active'),
(6, 2, 1, 0, 'Program relevance', 'Program relevance', 0, 4, 4, '2014-07-08 15:39:03', '2014-07-08 15:45:03', 'active'),
(7, 2, 1, 0, 'Project Administration', 'Project Administration', 0, 5, 1, '2014-07-08 15:39:23', '2014-07-08 15:45:19', 'active'),
(8, 0, 1, 0, 'Overall Customer Service Rating', 'Overall Customer Service Rating', 0, 3, 4, '2014-07-08 15:39:44', '2014-07-08 15:45:33', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(80) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `group_id`, `team_id`, `username`, `password`, `first_name`, `last_name`, `created`, `modified`, `status`) VALUES
(1, 1, 0, 'admin', 'Password1', 'Albert', 'Mananay', '2014-04-30 16:36:01', '2014-06-23 08:46:23', 0),
(2, 2, 0, 'dencio', 'Password1', 'Dennis', 'Abasa', '2014-05-06 21:09:54', '2014-05-06 21:09:54', 0),
(4, 3, 0, 'basar', 'Password1', 'Roel', 'Abasa', '2014-06-23 08:54:12', '2014-07-01 12:37:21', 0),
(5, 4, 0, 'sm', 'Password1', 'Joel', 'Hayag', '2014-07-01 12:38:48', '2014-07-02 13:25:59', 0),
(6, 5, 0, 'broker', 'Password1', 'John Paul', 'Paquibot', '2014-07-01 12:39:43', '2014-07-01 12:39:43', 0),
(7, 6, 0, 'agent007', 'Password1', 'James', 'Bond', '2014-07-01 12:40:11', '2014-07-01 12:40:11', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
