<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 * @property Group $Group
 * @property PaymentOption $PaymentOption
 * @property Team $Team
 */
class User extends AppModel {

	public $contain = array('Researcher','Tester');
	//public $order = array('User.id'=>'desc'); 

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Group' => array(
			'className' => 'Group',
			'foreignKey' => 'group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'LoginLog' => array(
			'className' => 'LoginLog',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => array('LoginLog.id'=>'desc'),
			'limit' => '10',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	/**
 * Validation parameters
 *
 * @var array
 */
	public $validate = array(
		'username' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'required' => true, 'allowEmpty' => false,
				'message' => 'Please enter a username.'),
			/*'alpha' => array(
				'rule' => array('alphaNumeric'),
				'message' => 'The username must be alphanumeric.'),*/
			'unique_username' => array(
				'rule' => array('isUnique', 'username'),
				'message' => 'This username is already in use.'),
			/*'username_min' => array(
				'rule' => array('minLength', '3'),
				'message' => 'The username must have at least 3 characters.')*/
		),
		'first_name' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'required' => true, 'allowEmpty' => false,
				'message' => 'Please enter your firstname.') 
		),
		'last_name' => array(
			'required' => array(
				'rule' => array('notEmpty'),
				'required' => true, 'allowEmpty' => false,
				'message' => 'Please enter your lastname.') 
		),
		'password' => array(
			'too_short' => array(
				'rule' => array('minLength', '6'),
				'message' => 'The password must have at least 6 characters.'),
			'required' => array(
				'rule' => 'notEmpty',
				'message' => 'Please enter a password.')),
		'confirm_password' => array(
			'rule' => 'confirmPassword',
			'message' => 'The passwords are not equal, please try again.')
	);

	public $actsAs = array('Acl' => array('type' => 'requester'));

	public function __construct($id = false, $table = null, $ds = null) {
	    parent::__construct($id, $table, $ds);
	    $this->virtualFields['name'] = sprintf('CONCAT(%s.first_name, " ", %s.last_name)', $this->alias, $this->alias);	    
	    $this->displayField = 'name';
	}

    public function parentNode() {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        if (isset($this->data['User']['group_id'])) {
            $groupId = $this->data['User']['group_id'];
        } else {
            $groupId = $this->field('group_id');
        }
        if (!$groupId) {
            return null;
        } else {
            return array('Group' => array('id' => $groupId));
        }
    }

    public function bindNode($user) {
	    return array('model' => 'Group', 'foreign_key' => $user['User']['group_id']);
	}

	/**
 * Custom validation method to ensure that the two entered passwords match
 *
 * @param string $password Password
 * @return boolean Success
 */
	public function confirmPassword($password = null) {
		if ((isset($this->data[$this->alias]['password']) && isset($password['confirm_password']))
			&& !empty($password['confirm_password'])
			&& ($this->data[$this->alias]['password'] === $password['confirm_password'])) {
			return true;
		}
		return false;
	}

	public function listOptions(){

		$params['conditions']['Group.name'] = $this->alias;
		$params['contain'] = array('Group');
		$params['fields'] = array($this->alias.'.id',$this->alias.'.name');
		$lists = $this->find('all',$params);
		$options = array('');
		foreach ($lists as $key => $value) {
			$options[$value[$this->alias]['id']] = $value[$this->alias]['name'];
		}

		return $options;
	}
}
