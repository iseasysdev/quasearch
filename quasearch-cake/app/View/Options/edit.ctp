<div class="options form">
<?php echo $this->Form->create('Option',array('class'=>'form-horizontal')); ?>
	<fieldset>
		<legend><?php echo __('Edit Option'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('title');
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>