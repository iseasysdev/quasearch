<div class="questionItems form">
<?php echo $this->Form->create('QuestionItem',array('class'=>'form-horizontal')); ?>
	<fieldset>
		<legend><?php echo __('Add Question Item'); ?></legend>
	<?php
		echo $this->Form->input('parent_id',array('options'=>$parentQuestionItems));
		echo $this->Form->input('question_id');
		echo $this->Form->input('title');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
