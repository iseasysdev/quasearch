<div class="questionItems form">
<?php echo $this->Form->create('QuestionItem'); ?>
	<fieldset>
		<legend><?php echo __('Edit Question Item'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('parent_id',array('options'=>$parentQuestionItems));
		echo $this->Form->input('question_id');
		echo $this->Form->input('title');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('QuestionItem.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('QuestionItem.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Question Items'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Question Items'), array('controller' => 'question_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Parent Question Item'), array('controller' => 'question_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
