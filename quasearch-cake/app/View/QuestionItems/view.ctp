<div class="questionItems view">
<h2><?php echo __('Question Item'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($questionItem['QuestionItem']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Parent Question Item'); ?></dt>
		<dd>
			<?php echo $this->Html->link($questionItem['ParentQuestionItem']['title'], array('controller' => 'question_items', 'action' => 'view', $questionItem['ParentQuestionItem']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Question'); ?></dt>
		<dd>
			<?php echo $this->Html->link($questionItem['Question']['id'], array('controller' => 'questions', 'action' => 'view', $questionItem['Question']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($questionItem['QuestionItem']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($questionItem['QuestionItem']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($questionItem['QuestionItem']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Question Item'), array('action' => 'edit', $questionItem['QuestionItem']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Question Item'), array('action' => 'delete', $questionItem['QuestionItem']['id']), null, __('Are you sure you want to delete # %s?', $questionItem['QuestionItem']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Question Items'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question Item'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Question Items'), array('controller' => 'question_items', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Parent Question Item'), array('controller' => 'question_items', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Questions'), array('controller' => 'questions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Question'), array('controller' => 'questions', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Question Items'); ?></h3>
	<?php if (!empty($questionItem['ChildQuestionItem'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Parent Id'); ?></th>
		<th><?php echo __('Question Id'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($questionItem['ChildQuestionItem'] as $childQuestionItem): ?>
		<tr>
			<td><?php echo $childQuestionItem['id']; ?></td>
			<td><?php echo $childQuestionItem['parent_id']; ?></td>
			<td><?php echo $childQuestionItem['question_id']; ?></td>
			<td><?php echo $childQuestionItem['title']; ?></td>
			<td><?php echo $childQuestionItem['created']; ?></td>
			<td><?php echo $childQuestionItem['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'question_items', 'action' => 'view', $childQuestionItem['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'question_items', 'action' => 'edit', $childQuestionItem['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'question_items', 'action' => 'delete', $childQuestionItem['id']), null, __('Are you sure you want to delete # %s?', $childQuestionItem['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Child Question Item'), array('controller' => 'question_items', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
