<div class="surveyForms view">
<h2><?php echo __('Survey Form'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($surveyForm['SurveyForm']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Survey'); ?></dt>
		<dd>
			<?php echo $this->Html->link($surveyForm['Survey']['title'], array('controller' => 'surveys', 'action' => 'view', $surveyForm['Survey']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($surveyForm['User']['name'], array('controller' => 'users', 'action' => 'view', $surveyForm['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($surveyForm['SurveyForm']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($surveyForm['SurveyForm']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($surveyForm['SurveyForm']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($surveyForm['SurveyForm']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($surveyForm['SurveyForm']['status']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Survey Form'), array('action' => 'edit', $surveyForm['SurveyForm']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Survey Form'), array('action' => 'delete', $surveyForm['SurveyForm']['id']), null, __('Are you sure you want to delete # %s?', $surveyForm['SurveyForm']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Survey Forms'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Survey Form'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Surveys'), array('controller' => 'surveys', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Survey'), array('controller' => 'surveys', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
