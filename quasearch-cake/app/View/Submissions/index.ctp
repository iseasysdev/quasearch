<div class="submissions index">
	<h2><?php echo __('Submissions'); ?></h2>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('enumerator'); ?></th>		
			<th><?php echo $this->Paginator->sort('latitude'); ?></th>
			<th><?php echo $this->Paginator->sort('longitude'); ?></th>
			<th><?php echo $this->Paginator->sort('map_api_url'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
		</tr>
	<?php foreach ($submissions as $submission): ?>
	<tr>
		<td><?php echo h($submission['Submission']['id']); ?>&nbsp;</td>
		<td style="white-space:nowrap">
			<?php echo $submission['Submission']['enumerator']; ?>
		</td>		
		<td><?php echo h($submission['Submission']['latitude']); ?>&nbsp;</td>
		<td><?php echo h($submission['Submission']['longitude']); ?>&nbsp;</td>
		<td><?php echo $this->Html->link($submission['Submission']['map_api_url']); ?>&nbsp;</td>
		<td><?php echo h($submission['Submission']['created']); ?>&nbsp;</td>
		<td><?php echo h($submission['Submission']['modified']); ?>&nbsp;</td>
		<td><?php echo h($submission['Submission']['status']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $submission['Submission']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $submission['Submission']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $submission['Submission']['id']), null, __('Are you sure you want to delete # %s?', $submission['Submission']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<?php echo $this->element('paging');?>
</div>