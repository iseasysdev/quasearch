<?php if($this->Paginator->counter('{:pages}') > 1):?>
<?php $span = isset($span) ? $span : 8; ?>
<?php $page = isset($this->request->params['named']['page']) ? $this->request->params['named']['page'] : 1; ?>
	<div class="pull-right">
		<?php
			$limit = (empty($this->request->query['limit']))?10:$this->request->query['limit'];
			$options = array( 10 => '10', 50 => '50', 100 => '100',200 => '200', 500 => '500',1000 => '1000' );

			echo $this->Form->create(array('type'=>'get','url' => array('controller' =>$this->request->params['controller'], 'action' => $this->request->params['action'])));		
			$this->request->params['named']['page'] = 1;	
			foreach ($this->request->query as $key => $value) {
				if($key=='limit') continue;
				echo $this->Form->hidden($key,array('value'=>$value));
			}

			echo $this->Form->select('limit', $options, array(
			    'value'=>$limit, 
			    'default'=>20, 
			    'empty' => FALSE, 
			    'onChange'=>'this.form.submit();', 
			    'name'=>'limit'
			    )
			);
			echo $this->Form->end();
		?>
	</div>
	<ul class="pagination">
		<?php echo $this->Paginator->prev(
			'&larr; ' . __('Previous'),
			array(
				'escape' => false,
				'tag' => 'li'
			),
			'<a onclick="return false;">&larr; Previous</a>',
			array(
				'class'=>'disabled prev',
				'escape' => false,
				'tag' => 'li'
			)
		);?>
		
		<?php $count = $page + $span; ?>
		<?php $i = $page - $span; ?>
		<?php while ($i < $count): ?>
			<?php $options = ''; ?>
			<?php if ($i == $page): ?>
				<?php $options = ' class="active"'; ?>
			<?php endif; ?>
			<?php if ($this->Paginator->hasPage($i) && $i > 0): ?>
				<li<?php echo $options; ?>><?php echo $this->Html->link($i, array("page" => $i,'?'=>$this->request->query)); ?></li>
			<?php endif; ?>
			<?php $i += 1; ?>
		<?php endwhile; ?>
		
		<?php echo $this->Paginator->next(
			__('Next') . ' &rarr;',
			array(
				'escape' => false,
				'tag' => 'li'
			),
			'<a onclick="return false;">Next &rarr;</a>',
			array(
				'class' => 'disabled next',
				'escape' => false,
				'tag' => 'li'
			)
		);?>
	</ul>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
<?php elseif($this->Paginator->counter('{:count}') == 0):?>
	<div class="alert alert-warning">No results found.</div>
<?php endif;?>