  <div data-role="header"  id="hdrSurvey" name="hdrSurvey" data-nobackbtn="false">
    <h1><?php echo $survey['Survey']['title'];?></h1>
  </div>
  <?php echo $this->Form->create('Survey', array('id' => 'SurveyForm','class'=>'form-horizontal','url'=>array('controller'=>'submissions','action'=>'add'))); ?> 
  <?php echo $this->Form->hidden('Submission.survey_id',array('value'=>$survey['Survey']['id'],'name'=>'survey_id'));?>
  <?php echo $this->Form->hidden('Submission.latitude',array('value'=>'','id'=>'latitude','name'=>'latitude'));?>
  <?php echo $this->Form->hidden('Submission.longitude',array('value'=>'','id'=>'longitude','name'=>'longitude'));?>
  <?php echo $this->Form->hidden('Submission.map_api_url',array('value'=>'','id'=>'map_api_url','name'=>'map_api_url'));?>

  <ol>
  <?php foreach ($forms as $key => $form):?>
    <li class="step" id="<?php echo $form['SurveyForm']['id'];?>">
      <?php echo $form['SurveyForm']['title'];?>
      <?php if(!empty($form['forms'])):?>
        <ol >
          <?php foreach($form['forms'] as $form):?>
            <li type="square">
              <?php echo $form['SurveyForm']['title'];?>
              <ul style="list-style: none outside none;">
                <?php foreach ($form['Question'] as $num => $question):?>
                  <li >
                    <?php if($question['type']=='Short Answer'):?>
                      <?php echo $this->Form->input('answer['.$question['id'].']',array('name'=>'answer['.$question['id'].']','label'=>$question['question'],'required'=>'required'));?>
                      <div class="clearfix"></div>
                    <?php elseif($question['type']=='Long Answer'):?>
                      <div data-role="fieldcontain" class="ui-field-contain ui-body ui-br">
                        <label for="what" id="whatLabel" class="ui-input-text"><?php echo $question['question'];?>:</label>
                        <textarea name="<?php echo 'answer['.$question['id'].']';?>" id="what" rows="8" cols="40" class="ui-input-text ui-body-null ui-corner-all ui-shadow-inset ui-body-c"></textarea>
                      </div>
                      <div class="clearfix"></div>
                    <?php elseif($question['type']=='Multiple Items'):?>
                      <?php echo $question['question'];?>
                      <div class="col-lg-offset-1 col-lg-10">
                        <table class="table table-bordered">                
                          <tr>
                            <th>Items</th>
                            <?php foreach ($question['Option']['OptionItem'] as $key => $item):?>
                              <th><?php echo $item['value'];?></th>
                            <?php endforeach;?>
                          </tr>               
                          <?php foreach ($question['QuestionItem'] as $key => $question_item):?>
                            <tr>
                              <th style="white-space:nowrap"><?php echo $question_item['title'];?></th>
                              <?php foreach ($question['Option']['OptionItem'] as $key => $item):?>
                                <?php if(count($question_item['ChildQuestionItem']) < 1):?>
                                <th>
                                  <?php //echo $this->Form->input('answer['.$question['id'].']['.$question_item['id'].']['.$item['id'].']',array('name'=>'answer['.$question['id'].']['.$question_item['id'].']['.$item['id'].']','label'=>$question['question'],'required'=>'required','label'=>false,'div'=>false));?>
                                  <select name="<?php echo 'answer['.$question['id'].']['.$question_item['id'].']['.$item['id'].']';?>" >
                                    <option value="No">No</option>
                                    <option value="Yes">Yes</option>
                                  </select> 
                                </th>
                                <?php endif;?>
                              <?php endforeach;?>
                            </tr>
                            <?php foreach ($question_item['ChildQuestionItem'] as $key => $question_subitem):?>
                              <tr>
                                <th style="padding-left:30px;white-space:nowrap"><?php echo $question_subitem['title'];?></th>
                                <?php foreach ($question['Option']['OptionItem'] as $key => $item):?>
                                  <th>
                                    <?php //echo $this->Form->input('answer['.$question['id'].']['.$question_subitem['id'].']['.$item['id'].']',array('name'=>'answer['.$question['id'].']['.$question_subitem['id'].']['.$item['id'].']','label'=>$question['question'],'required'=>'required','label'=>false,'div'=>false));?> 
                                    <select name="<?php echo 'answer['.$question['id'].']['.$question_subitem['id'].']['.$item['id'].']';?>" >
                                    <option value="No">No</option>
                                    <option value="Yes">Yes</option>
                                  </select>
                                  </th>
                                <?php endforeach;?>
                              </tr>
                            <?php endforeach;?>
                          <?php endforeach;?>
                          
                        </table>
                      </div>
                      <div class="clearfix"></div>
                    <?php elseif($question['type']=='Multiple Choice'):?>
                      <?php echo $question['question'];?>
                      <div class="col-lg-12">
                        <table class="table table-bordered">
                          <tr>
                            <?php $perc = (100 / count($question['Option']['OptionItem'])).'%';?>
                          <?php foreach ($question['Option']['OptionItem'] as $key => $option):?>
                            <?php $option_id = $question['id'].'-'.$option['id'];?>
                            <td style="width:<?php echo $perc;?>;text-align:center"><input id="<?php echo $option_id;?>" type="radio" name="answer[<?php echo $question['id'];?>]" value="<?php echo $option['value'];?>" /></td>
                          <?php endforeach;?>
                          </tr>
                          <tr>
                            <?php $perc = (100 / count($question['Option']['OptionItem'])).'%';?>
                          <?php foreach ($question['Option']['OptionItem'] as $key => $option):?>
                            <?php $option_id = $question['id'].'-'.$option['id'];?>
                            <td style="width:<?php echo $perc;?>;text-align:center"><label for="<?php echo $option_id;?>"><?php echo $option['value'];?></label></td>
                          <?php endforeach;?>
                          </tr>
                        </table>
                      </div>
                    <?php endif;?>
                  </li>
                <?php endforeach;?>
              </ul>
            </li>
          <?php endforeach;?>
        </ol>
      <?php else:?>
        <ul style="list-style: none outside none;">
          <?php foreach ($form['Question'] as $num => $question):?>
            <li>
              <?php if($question['type']=='Short Answer'):?>
                <div data-role="fieldcontain" id="shipDiv" class="ui-field-contain ui-body ui-br">
                  <label for="shipNo" class="ui-input-text"><?php echo $question['question'];?></label>    
                  <input type="text" name="<?php echo 'answer['.$question['id'].']';?>" id="shipNo" class="ui-input-text ui-body-null ui-corner-all ui-shadow-inset ui-body-c" required="required">
                </div>
                <div class="clearfix"></div>
              <?php elseif($question['type']=='Long Answer'):?>                
                  <div data-role="fieldcontain" id="shipDiv" class="ui-field-contain ui-body ui-br">
                    <label for="shipNo" class="ui-input-text"><?php echo $question['question'];?></label>    
                    <textarea name="<?php echo 'answer['.$question['id'].']';?>" required="required"></textarea>
                  </div>
                <div class="clearfix"></div>
              <?php elseif($question['type']=='Multiple Items'):?>
                <?php echo $question['question'];?>
                <div class="col-lg-offset-1 col-lg-10">
                  <table class="table table-bordered">                
                    <tr>
                      <th>Items</th>
                      <?php foreach ($question['Option']['OptionItem'] as $key => $item):?>
                        <th><?php echo $item['value'];?></th>
                      <?php endforeach;?>
                    </tr>               
                    <?php foreach ($question['QuestionItem'] as $key => $question_item):?>
                      <tr>
                        <th style="white-space:nowrap"><?php echo $question_item['title'];?></th>
                        <?php foreach ($question['Option']['OptionItem'] as $key => $item):?>
                          <?php if(count($question_item['ChildQuestionItem']) < 1):?>
                          <th>
                            <?php //echo $this->Form->input('answer['.$question['id'].']['.$question_item['id'].']['.$item['id'].']',array('name'=>'answer['.$question['id'].']['.$question_item['id'].']['.$item['id'].']','label'=>$question['question'],'required'=>'required','label'=>false,'div'=>false));?>
                              <div data-role="fieldcontain">
                                <select name="<?php echo 'answer['.$question['id'].']['.$question_item['id'].']['.$item['id'].']';?>" id="flip-c" data-role="slider" data-theme="b">
                                  <option value="no">No</option>
                                  <option value="yes">Yes</option>
                                </select> 
                              </div>
                          </th>
                          <?php endif;?>
                        <?php endforeach;?>
                      </tr>
                      <?php foreach ($question_item['ChildQuestionItem'] as $key => $question_subitem):?>
                        <tr>
                          <th style="padding-left:30px;white-space:nowrap"><?php echo $question_subitem['title'];?></th>
                          <?php foreach ($question['Option']['OptionItem'] as $key => $item):?>
                            <th>
                              <?php //echo $this->Form->input('answer['.$question['id'].']['.$question_subitem['id'].']['.$item['id'].']',array('name'=>'answer['.$question['id'].']['.$question_subitem['id'].']['.$item['id'].']','label'=>$question['question'],'required'=>'required','label'=>false,'div'=>false));?>
                              <div data-role="fieldcontain">
                                <select name="<?php echo 'answer['.$question['id'].']['.$question_subitem['id'].']['.$item['id'].']';?>" id="flip-c" data-role="slider" data-theme="b">
                                  <option value="no">No</option>
                                  <option value="yes">Yes</option>
                                </select> 
                              </div>
                            </th>
                          <?php endforeach;?>
                        </tr>
                      <?php endforeach;?>
                    <?php endforeach;?>
                    
                  </table>
                </div>
              <?php elseif($question['type']=='Multiple Choice'):?>
                <?php echo $question['question'];?>
                <div class="col-lg-12">
                  <table class="table table-bordered">
                    <tr>
                      <?php $perc = (100 / count($question['Option']['OptionItem'])).'%';?>
                    <?php foreach ($question['Option']['OptionItem'] as $key => $option):?>
                      <?php $option_id = $question['id'].'-'.$option['id'];?>
                      <td style="width:<?php echo $perc;?>;text-align:center">
                        <input id="<?php echo $option_id;?>" type="radio" name="answer[<?php echo $question['id'];?>]" value="<?php echo $option['value'];?>" /></td>
                    <?php endforeach;?>
                    </tr>
                    <tr>
                      <?php $perc = (100 / count($question['Option']['OptionItem'])).'%';?>
                    <?php foreach ($question['Option']['OptionItem'] as $key => $option):?>
                      <?php $option_id = $question['id'].'-'.$option['id'];?>
                      <td style="width:<?php echo $perc;?>;text-align:center"><label for="<?php echo $option_id;?>"><?php echo $option['value'];?></label></td>
                    <?php endforeach;?>
                    </tr>
                  </table>
                </div>
              <?php endif;?>
            </li>
          <?php endforeach;?>
        </ul> 
      <?php endif;?> 
      
    </li>
  
  <?php endforeach;?>  
  </ol>

  <div class="clearfix"></div><br/>
  <div id="demoNavigation" class="pull-right">              
    <table class="">
      <tr>
        <!-- <td><input class="navigation_button btn btn-default" id="back" value="Back" type="reset" /></td>-->
        <td><input class="navigation_button btn btn-default" id="next" value="Submit" type="submit" /></td>
      </tr>
    </table>
  </div>
  <div class="clearfix"></div>

  <!-- contentTransition is displayed after the form is submitted until a response is received back. -->
  <div data-role="content" id="contentTransition" name="contentTransition" style="display:none"> 
   <div align="CENTER"><h4>Your claim has been sent. Please wait.</h4></div>
   <div align="CENTER"><img id="spin" name="spin" src="img/wait.gif"/></div>
  </div>  <!-- contentTransition -->
  
  <!-- Although stored within page1 div tag, hdrConfirmation, contentConfirmation and ftrConfirmation represent a self contained 'confirmation page' -->
  <div data-role="header"  id="hdrConfirmation" name="hdrConfirmation" data-nobackbtn="false" style="display:none">
    <h1>Submission Created</h1>
  </div>  
  <div data-role="content" id="contentConfirmation" name="contentConfirmation" align="center" style="display:none">   
    <p>Your responses have been submitted.</p> 
    <p>Thanks!</p>
  </div><!-- contentConfirmation -->  



  <script type="text/javascript">
  /*
    function success(position){
      var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      var myOptions = {
      zoom: 15,
      center: latlng,
      mapTypeControl: false,
      navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
      mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      var map = new google.maps.Map(document.getElementById("mapcanvas"), myOptions);
      var marker = new google.maps.Marker({
      position: latlng,
      map: map,
      title:"You are here!"
      });
      $.cookie("MyLat", position.coords.latitude); // Storing latitude value
      $.cookie("MyLon", position.coords.longitude); // Storing longitude value
    }

    function error(msg){}
    
    if (navigator.geolocation){
      navigator.geolocation.getCurrentPosition(success, error);
    }

    //Jquery Code
    $(document).ready(function(){
      var lat = $.cookie("MyLat");
      var lon = $.cookie("MyLon");
      var url="http://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lon+"&sensor=false";
      alert('Google Map API: '+url);
      //Get Json Request Here

    });*/
    </script>

<script type="text/javascript">
  $(document).ready(function(){
    /*
    $("#SurveyForm").submit(function(){
      var lat = $.cookie("MyLat");
      var lon = $.cookie("MyLon");
      $("#latitude").val(lat);
      $("#longitude").val(lon);
      console.debug(lat);
      console.debug($("#latitude").val());

      return true;
    });

    $("#SurveyForm").validate();
    */

    /*$("#SurveyForm").formwizard({ 
      formPluginEnabled: true,
      validationEnabled: true,
      focusFirstInput : true,
      disableUIStyles : true,
      formOptions :{
        success: function(data){
          console.debug('success'); 
        },
        beforeSubmit: function(data){
          $("#contentConfirmation").html("data sent to the server: " + $.param(data));
          $("#contentConfirmation").show();
        },
        dataType: 'json',
        resetForm: true
      } 
     }
    );*/

  var Geo={};

  if (navigator.geolocation) {
     navigator.geolocation.getCurrentPosition(success, error);
  }

  //Get the latitude and the longitude;
  function success(position) {
      Geo.lat = position.coords.latitude;
      Geo.lng = position.coords.longitude;
      populateHeader(Geo.lat, Geo.lng);
  }

  function error(){
      console.log("Geocoder failed");
  }

  function populateHeader(lat, lng){
    alert('latitude generated'+lat)
      $("#latitude").val(lat);      
      $("#longitude").val(lng);

      var url="http://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lng+"&sensor=false";
      $("#map_api_url").val(url);
  }

  });
</script>