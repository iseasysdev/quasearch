<div class="optionItems view">
<h2><?php echo __('Option Item'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($optionItem['OptionItem']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Option'); ?></dt>
		<dd>
			<?php echo $this->Html->link($optionItem['Option']['title'], array('controller' => 'options', 'action' => 'view', $optionItem['Option']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Value'); ?></dt>
		<dd>
			<?php echo h($optionItem['OptionItem']['value']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($optionItem['OptionItem']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($optionItem['OptionItem']['modified']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($optionItem['OptionItem']['status']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Option Item'), array('action' => 'edit', $optionItem['OptionItem']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Option Item'), array('action' => 'delete', $optionItem['OptionItem']['id']), null, __('Are you sure you want to delete # %s?', $optionItem['OptionItem']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Option Items'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Option Item'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Options'), array('controller' => 'options', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Option'), array('controller' => 'options', 'action' => 'add')); ?> </li>
	</ul>
</div>
