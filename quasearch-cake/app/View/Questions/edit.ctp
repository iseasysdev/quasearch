<div class="questions form">
<?php echo $this->Form->create('Question',array('class'=>'form-horizontal')); ?>
	<fieldset>
		<legend><?php echo __('Edit Question'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('survey_form_id');		
		echo $this->Form->input('type',array('id'=>'type'));
		echo $this->Form->input('option_id',array('id'=>'option'));
		echo $this->Form->input('question',array('type'=>'textarea'));
		echo $this->Form->input('status');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>

<script type="text/javascript">
	$("#type").change(function(){
		toggleOption();
	});
	toggleOption();

	function toggleOption(){
		if($("#type").val()=='Multiple Choice' || $("#type").val()=='Multiple Items'){
			$("#option").parent().show();
			$("#option").removeAttr('disabled');
		}else{
			$("#option").parent().hide();
			$("#option").attr('disabled','disabled');
		}
	}
</script>