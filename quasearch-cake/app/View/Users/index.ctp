<h1>Users</h1>
<div class="pull-right">
	<a href="<?php echo $this->Html->url(array('action'=>'add'));?>" class="btn btn-primary btn-lg">
	  <i class="icon-plus-sign"></i> Add New User
	</a>
</div>

<div class="clearfix"></div>
<?php if(count($users) > 0):?>
<table class="table table-bordered table-striped">
	<thead>
	  	<tr>
	  	  <th>ID</th>
	  	  <th>Username</th>
	  	  <th>First Name</th>
	  	  <th>Last Name</th>
	  	  <th>Admin Type</th>
	  	  <th>Created</th>
		  <th>Actions</th>
	  	</tr>
	</thead>   
	<tbody>
		<?php foreach($users as $user):?>
		<tr>
			<td class="center"><?php echo $user['User']['id'];?></td>			
			<td class="center"><?php echo $user['User']['username'];?></td>			
			<td class="center"><?php echo $user['User']['first_name'];?></td>
			<td class="center"><?php echo $user['User']['last_name'];?></td>
			<td class="center"><?php echo $user['Group']['name'];?></td>
			<td><?php echo $this->Time->format('M jS, Y', $user['User']['created']);?></td>
			<td class="center" nowrap>

				<a class="btn btn-info" href="<?php echo $this->Html->url(array('action'=>'edit',$user['User']['id']));?>">
					<i class="icon-pencil"></i>  
				</a>
				<?php echo $this->Form->postLink(__('<i class="icon-trash "></i> '), array('action' => 'delete', $user['User']['id']), array('class'=>'btn btn-danger','escape'=>false), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
			</td>
			
		</tr>
		<?php endforeach;?>
	</tbody>
</table>
<?php endif;?>