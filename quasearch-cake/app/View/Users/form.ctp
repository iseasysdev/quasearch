<div class="users form">
<?php echo $this->Form->create('User',array('class'=>'form-horizontal')); ?>
	<fieldset>
		<legend><?php echo __('Edit User'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('group_id',array('label'=>'Admin Type'));
		echo $this->Form->input('username');
		echo $this->Form->input('first_name');
		echo $this->Form->input('last_name');
		echo $this->Form->input('password',array('after'=>!empty($this->request->data['User']['id'])?'Leave blank to keep the current password':'','value'=>'','autocomplete'=>false,'required'=>false));
		echo $this->Form->input('confirm_password',array('type'=>'password','value'=>'','autocomplete'=>false,'required'=>false));
		echo $this->Form->input('status');
		
	?>
	</fieldset>
	<?php echo $this->Form->end(array('class'=>'btn btn-primary btn-lg','label'=>'Submit')); ?>
</div>