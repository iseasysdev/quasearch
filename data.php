<?php

$members[] = array('Alberto A. Mananay Jr.','Cebu City','20,000');
$members[] = array('Rodel G. Revilla','Cebu City','20,000');
$members[] = array('Shawn Euclid Gandhi P. Espina','135 Juana Osmena St. Cebu City','28,000','bod');
$members[] = array('Jacqueline Jabonero Espina','135 Juana Osmena St. Cebu City','28,000','bod');
$members[] = array('Dinah Penecitos Craven','8th St. Mary\'s Drive, Brgy Apas, Cebu City','28,000','bod');
$members[] = array('Doreen Jabonero Garcia','Villa San Lorenzo Guadalupe, Cebu City','28,000');
$members[] = array('Adinah Ralph','Villa San Lorenzo Guadalupe, Cebu City','28,000','associate');

$members[] = array('Pedro Bohol','53 Sunrise Village, Pardo Cebu City','28,000');
$members[] = array('Christopher Fadriga','#29 Diamond Circle St., St. Michael\'s Villa, Kasambagan, Cebu City','28,000','bod');
$members[] = array('Lim Tek AN Ko','Cebu City','28,000');
$members[] = array('Genevieve Penecitos P. Belciña','21 Countryside Subd., Canduman Mandaue City','28,000');
$members[] = array('Julieto E. Enolpe','4285 Ward II, Poblacion, Minglanilla, Cebu','28,000');
$members[] = array('Eduardo Dujon','Lipata, Minglanilla, Cebu','28,000');
$members[] = array('Anne Arteche','Talamban, Cebu City','28,000');
$members[] = array('Wilma Akamatsu','G-1 Jade St., Casals Village, Mabolo, Cebu City','28,000','bod');
$members[] = array('Jaime Jose Ligot Escaño','Cebu City','28,000');
$members[] = array('Carlito Cabellon','214 V. Rama Avenue, Cebu City','28,000');
$members[] = array('Rene Pamintuan','Cebu City','28,000');


$products[] = array('Product 1','180.00');
$products[] = array('Product 2','125.00');
$products[] = array('Product 3','10.00');
$products[] = array('Product 4','30.00');
$products[] = array('Product 5','50.00');
$products[] = array('Product 6','70.00');
$products[] = array('Product 7','150.00');
$products[] = array('Product 8','280.00');
$products[] = array('Product 9','100.00');
$products[] = array('Product 10','80.00');

$tribe[] = array('yellow');

$unit_types = array('meters', 'feet', 'kilogram', 'ounces', 'pounds', 'yards', 'packs');