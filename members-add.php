<?php include "header.php";?>

	
	<?php include "nav.php";?>
	<div class="container">
		<div class="row"> 
			<!-- start: Main Menu -->
			<?php include 'menu.php';?>
			<!-- end: Main Menu -->
			<!-- start: Content -->
			<div id="content" class="col-sm-11 padding0">
                <div class="form-member">
				<div class="row">
					<div class="col-lg-12">
                        <div class="panel panel-primary">
						  	<div class="panel-heading">
						    	<h3 class="panel-title"><span class="glyphicons group"><i></i></span>Sto. Nino Subdivision  Homeowners 

						    	</h3>
						  	</div>
						  	<div class="panel-body">
						  		<!-- add member form-->
                                <h2>Add New Homeowner</h2>
                                <form action="/" method="post">
                                    <section class="section institution-info">
                                        <h3>Basic Profile<small>Basic information of the Member you are adding.</small></h3>
                                        <div class="well">
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <div class="media">
                                                    <img src="http://placehold.it/160x160">
                                                </div>
                                            </div>
                                            <div class="col-lg-10 form-group padding0">
                                            <div class="col-lg-12 padding0">
                                                <div class="col-lg-4 padding0">
                                                    <label>Tribe</label>
                                                    <select class="form-control col-lg-4">
                                                        <option>Yellow</option>
                                                        <option>Blue</option>
                                                    </select>
                                                </div>
                                                <div class="clearfix">&nbsp;</div>
                                                <div>
                                                    <div class="col-lg-4 padding5">
                                                        <div class="form-group">
                                                            <label for="institution-name">
                                                                Family Name
                                                            </label>
                                                            <input type="text" class="col-lg-12 form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4 padding5">
                                                        <div class="form-group">
                                                            <label for="description">
                                                                First Name
                                                            </label>
                                                            <input type="text" class="col-lg-12 form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3 padding5">
                                                        <div class="form-group">
                                                            <label for="institution-name">
                                                                Middle Name
                                                            </label>
                                                            <input type="text" class="col-lg-12 form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-1 padding5">
                                                        <div class="form-group">
                                                            <label for="description">
                                                                Suffix
                                                            </label>
                                                            <input type="text" class="col-lg-12 form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                                
                                        </div>  
                                    </section>
                                    <section class="section address-info">
                                        <h3>
                                            Homeowner's Address
                                            <small>
                                                Complete Address of the Member that you are adding.
                                            </small>
                                        </h3>
                                        <div class="well">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <label>
                                                        Complete Address
                                                    </label>
                                                    <input type="text" name="address1" id="address1" placeholder="" class="col-lg-12 form-control">
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6 form-group">
                                                        <label>
                                                            City
                                                        </label>
                                                        <select class="form-control">
                                                            <option>Cebu</option>
                                                            <option>Mandaue</option>
                                                        </select>
                                                    </div> 
                                                          <div class="col-lg-6 form-group">
                                                        <label for="zip">
                                                            Zip / Postal Code
                                                        </label>
                                                        <input type="text" name="zip" id="zip" placeholder="" class="form-control col-lg-6">
                                                    </div>                                                   
                                                </div>
                             
                                            </div>
                                        </div>
                                        </div>
                                    </section>
                                    <section class="section contact-info">
                                        <h3>
                                            Contact Details
                                            <small>
                                                Member's contact details. Please be as specific as possible.
                                            </small>
                                        </h3>
                                        <div class="well">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="row terms">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label for="Phone Number">
                                                                Member's Email Address
                                                            </label>
                                                            <input type="text" name="Email-address" id="Email-address" placeholder=""
                                                            class="col-lg-12 form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <label for="Phone Number">
                                                                Phone Number
                                                            </label>
                                                            <input type="text" name="phone" id="phone" placeholder="" class="form-control col-lg-6">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </section>
                                    <section class="section household-listing">
                                        <h3>Household Listing
                                            <small>
                                                Relationship to the Member. Please be as specific as possible.
                                            </small>
                                        </h3>
                                        <div class="well">                                       
                                        <table class="table table-bordered">
                                          <thead class="primary">
                                            <tr>
                                              <th>Names</th>
                                              <th>Relationship</th>
                                              <th>Actions</th>
                                            </tr>
                                          </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div class="col-lg-12 padding0">
                                                            <div class="col-lg-6 padding0">  
                                                                <input type="text" placeholder="Lastname" class="form-control">
                                                            </div>
                                                            <div class="col-lg-6 padding0 firstname">
                                                                <input type="text" placeholder="Firstname" class="form-control">
                                                            </div>
                                                        </div>
                                                  </td>
                                                  <td> 
                                                    <div class="col-lg-12 padding0">
                                                        <div class="form-group">
                                                            <select class="form-control">
                                                                <option>Cebu</option>
                                                                <option>Mandaue</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <a class="btn btn-info" href="#">
                                                            <i class="fa fa-edit"></i>  
                                                        </a>
                                                        <a class="btn btn-danger" href="#">
                                                            <i class="fa fa-trash-o"></i>  
                                                        </a>
                                                    </td>
                                                </tr>
                                          </tbody>
                                        </table>

                                        <a href="add-household" class="btn btn-default">Add Household Member</a>
                                        </div>

                                    </section>
                                    <hr>
                                    <section class="section footer">
                                        <div class="row terms">
                                            <div class="col-lg-12">
                                                <a href="add-clinic-for-hospital-specializations.html" class="btn pull-right btn-primary btn-lg">
                                                   Submit Member
                                                </a>
                                            </div>
                                        </div>
                                    </section>
                                </form>
						    </div>
                        </div>
					</div><!--/col-->	
				</div>
                </div>	
			</div>
			<!-- end: Content -->
			
			<!-- start: Widgets Area -->
		</div><!--/row-->
	</div><!--/container-->
	<div class="clearfix"></div>
<?php include "footer.php";?>
